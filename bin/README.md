#/bin
This directory contains executable programs (typically shell scripts) that are used to provide minimal functionality for the purpose of starting/stopping applications, creating backups, etc.
