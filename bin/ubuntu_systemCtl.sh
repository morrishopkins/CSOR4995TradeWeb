#!/usr/bin/env bash

if [ -z "$RABBIT_PATH" ]; then
	echo "System specific environment must be sourced prior to starting system."
	echo "e.g. source bin/fc16_env.sh"
	exit 1
fi

CURR_DIR="$(pwd)"
pushd `dirname $0` > /dev/null
BIN_DIR=`pwd`
popd > /dev/null
ROOT="$BIN_DIR/.."
LOG_DIR="$ROOT/var/logs"
RUN_DIR="$ROOT/run"
DB_PATH="$ROOT/var/dbdata"
ETC_DIR="$ROOT/etc"
APP_DIR="$ROOT/apps"

function printUsage() {
	echo "Usage:";
	echo "`basename $0` [options]";
	echo "Options:";
	echo "    [--start] [--stop]";
	echo "    --start          Start mongodb, rabbitmq-server, fixclient, exchange, and tradeweb";
	echo "    --stop           Stop mongodb, rabbitmq-server, fixclient, exchange, and tradeweb";
	echo ;
	exit 0;
}

function adddate() {
    while IFS= read -r line; do
        echo "$(date) $line"
    done
}

function timestamp() {
	date +"%Y-%m-%d"
}

function cleanupStart() {
	echo "Cleaning up artifacts created durring start."
	cd "$CURR_DIR"
}

function startMongo() {
	echo "Starting mongodb."
	pushd "$ROOT" > /dev/null
	nohup mongod --dbpath "$DB_PATH" --logpath "$LOG_DIR/mongod_$(timestamp).log" 2>&1 &
	echo $! >> "$RUN_DIR/system_pids.txt"
	popd > /dev/null
}

function startFixClient() {
	# wait for rabbitmq to start.
	tail -f "$LOG_DIR/rabbitmq_server_$(timestamp).log" | while read LOGLINE
	do
  	[[ "${LOGLINE}" == *"Starting broker... completed"* ]] && pkill -P $$ tail
	done
	echo "RabbitMQ start completed."

	echo "Starting FIX Client."
	pushd "$APP_DIR/fixclient" > /dev/null
	nohup python ./fix.py >> "$LOG_DIR/fixclient_$(timestamp).log" 2>&1 &
	echo $! >> "$RUN_DIR/system_pids.txt"
	popd > /dev/null
}


function startExchange() {

	echo "Starting Exchange."
	#echo "Exchange startup not yet implemented."

	echo "RabbitMQ start completed."
	pushd "$APP_DIR/fixserver" > /dev/null
	nohup python ./server.py >> "$LOG_DIR/fixserver_$(timestamp).log" 2>&1 &
    echo $! >> "$RUN_DIR/system_pids.txt"
    popd > /dev/null

}

function startRabbitMQ() {
	echo "Starting RabbitMQ Server"
	pushd "$RABBIT_PATH" > /dev/null
	#if log already exists append it to back up and remove log.
	if [ -r "$LOG_DIR/rabbitmq_server_$(timestamp).log" ]; then
		cat "$LOG_DIR/rabbitmq_server_$(timestamp).log" >> "$LOG_DIR/rabbitmq_server_$(timestamp).log.bkup"
		rm "$LOG_DIR/rabbitmq_server_$(timestamp).log"
	fi
	nohup ./rabbitmq-server >> "$LOG_DIR/rabbitmq_server_$(timestamp).log" 2>&1 &
	echo $! > "$RUN_DIR/system_pids.txt"
	popd > /dev/null
}

function startTradeWeb() {
	echo "Starting TradeWeb."
	pushd "$ROOT" > /dev/null
	nohup python ./manage.py runserver >> "$LOG_DIR/tradeweb_$(timestamp).log" 2>&1 &
	echo $! >> "$RUN_DIR/system_pids.txt"
	popd > /dev/null
}

function startAll() {
	echo "Starting all services ..."
	startRabbitMQ
	startMongo
	startFixClient
	startExchange
	startTradeWeb
	cleanupStart
}

function stopAll() {
	echo "Stopping all services ..."
	kill -9 `cat "$RUN_DIR/system_pids.txt"`
	kill -9 `ps aux | grep rabbitmq | grep -v "\-\-color" | awk '{print $2}'`
	kill -9 `ps aux | grep manage.py | grep -v "\-\-color" | awk '{print $2}'`
	kill -9 `ps aux | grep fix.py | grep -v "\-\-color" | awk '{print $2}'`
	kill -9 `ps aux | grep mongod | grep -v "\-\-color" | awk '{print $2}'`
	rm "$RUN_DIR/system_pids.txt"
}

if [ "$1" == "-h" ] ; then
	printUsage;
fi

options=`getopt  --l stop,start,help  -- -n $0 "$@"`

if [ $? -ne 0 ] ; then
   printUsage
fi

set -- $options

for i
do
  case "$i" in

	--start)
		startAll;;
	--stop)
		stopAll;;
	--help)
		printUsage;
		;;
	--)
		break;;
    esac
  shift;
done


