#!/bin/bash

#start out by sourcing the appropriate python virtual environment.
pushd `dirname $0` > /dev/null
BIN_DIR=`pwd`
popd > /dev/null

source "$BIN_DIR/../fc16_venv/bin/activate"
export RABBIT_PATH="$BIN_DIR/../lib/rabbitmq_server-3.5.6_fedora/sbin"

