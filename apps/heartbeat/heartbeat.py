from time import sleep
import pika
import threading

SLEEP_TIME = 6
HEARTBEAT_MAX = 10


connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='tw_read_heart')
channel.queue_declare(queue='tw_resp_heart')
channel.queue_declare(queue='tw_failover')
channel.queue_declare(queue='fc_read_heart')
channel.queue_declare(queue='fc_resp_heart')
channel.queue_declare(queue='fc_failover')

TW_TOTAL=0
FC_TOTAL=0


def fc_heartbeat_heard(channel, method_frame, header_frame, body):
    global FC_TOTAL
    print 'FC_TOTAL: ', FC_TOTAL
    FC_TOTAL = 0
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)

def tw_heartbeat_heard(channel, method_frame, header_frame, body):
    global TW_TOTAL
    print 'TW_TOTAL: ', TW_TOTAL
    TW_TOTAL = 0
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)


def listen_all_heartbeats():
    try:
        channel.basic_consume(fc_heartbeat_heard, 'fc_resp_heart')
        channel.basic_consume(tw_heartbeat_heard, 'tw_resp_heart')
    except KeyboardInterrupt:
        print 'Interrupted'
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
    channel.start_consuming()



def send_all_heartbeats():
    global TW_TOTAL, FC_TOTAL
    print 'Inside send_all_heartbeats:'
    sleep(SLEEP_TIME)
    TW_ALIVE=True
    FC_ALIVE=True
    while TW_ALIVE or FC_ALIVE:
        try:
            if TW_ALIVE:
                channel.basic_publish(exchange='', routing_key='tw_read_heart', body='1')
                TW_TOTAL = TW_TOTAL + 1
                print 'TW_TOTAL: ', TW_TOTAL
            if FC_ALIVE:
                channel.basic_publish(exchange='', routing_key='fc_read_heart', body='1')
                FC_TOTAL = FC_TOTAL + 1
                print 'FC_TOTAL: ', FC_TOTAL

            # When we are unable to communicate with tradeweb we send a message to the fixclient
            # telling it that it needs to switch over to reading from the queue dedicated to the
            # passive tradeweb.
            if(TW_TOTAL > HEARTBEAT_MAX and TW_ALIVE == True):
                print 'Unable to communicate with primary tradeweb server. Failing over to backup.'
                channel.basic_publish(exchange='', routing_key='tw_failover', body='1')
                TW_ALIVE = False

            # vice-versa
            if(FC_TOTAL > HEARTBEAT_MAX and FC_ALIVE == True):
                print 'Unable to communicate with primary fixclient server. Failing over to backup.'
                channel.basic_publish(exchange='', routing_key='fc_failover', body='1')
                FC_ALIVE = False

        except KeyboardInterrupt:
            print 'Interrupted'
            try:
                sys.exit(0)
            except SystemExit:
                os._exit(0)
        sleep(SLEEP_TIME)

def main():
    tasks = [send_all_heartbeats, listen_all_heartbeats]
    for task in tasks:
        t = threading.Thread(target=task)
        t.start()

if __name__ == '__main__':
    main()
