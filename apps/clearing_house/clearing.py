import sys
import os
import time
import random
import getopt
from datetime import datetime
import pika
from ConfigParser import SafeConfigParser
from bs4 import BeautifulSoup
from string import Template


connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
CONFIRMED_THRESHOLD = 7 # a number between 1 and 10 to represent ratio of clearing.


def read_and_respond_from_xml(xml_string):
    ast = BeautifulSoup(xml_string, "xml")  # gives us ast of the xml string
    print ast
    print ast.clearingRequested
    uuid = ast.clearingRequested.trade.tradeId["uuid"]
    start_date = str(ast.swap.contract_dates.start_date.contents[0])
    termination_date = str(ast.swap.contract_dates.termination_date.contents[0])
    floating_rate = float(ast.find(id="floatLeg").rate["amnt"])
    spread = float(ast.swap.spread["amnt"])
    fixed_rate = float(ast.find(id="fixedLeg").rate["amnt"])
    fixed_payee = ast.find(id="fixedLeg").payee["name"]
    float_payee = ast.find(id="floatLeg").payee["name"]
    trader = ast.swap.trader["name"]
    transact_time = str(ast.trade.tradeHeader.transact_time.contents[0])

    print '^^^^^^^^^^^^^^^^^^^^^^^'
    print uuid
    print start_date
    print termination_date
    print str(floating_rate)
    print str(spread)
    print str(fixed_rate)
    print fixed_payee
    print float_payee
    print trader
    print transact_time
    print '$$$$$$$$$$$$$$$$$$$$$$$'

    z = random.randint(1, 10)
    print 'random value is: ', z
    if z < CONFIRMED_THRESHOLD:
        return respond_confirmed(uuid, start_date, termination_date, floating_rate, spread, fixed_rate, fixed_payee, float_payee, trader, transact_time)
    else:
        return respond_denied(uuid, start_date, termination_date, floating_rate, spread, fixed_rate, fixed_payee, float_payee, trader, transact_time)


def respond_confirmed(uuid, start_date, termination_date, floating_rate, spread, fixed_rate, fixed_payee, float_payee, trader, transaction_time):
    swapxml_template = Template(
        '''<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <tradeweb:FpML xmlns="http://www.fpml.org/FpML-5/confirmation" xmlns:tradeweb="" xmlns:dsig="http://www.w3.org/2000/09/xmldsig#">
        <clearingConfirmed fpmlVersion="5-0">
            <header>
                <!-- metadata on the message goes here. -->
            </header>
            <trade>
                <tradeHeader xsi:type="tradeweb:TWTradeHeader" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <execution>
                        <transact_time>$transaction_time</transact_time>
                    </execution>
                    <tradeId uuid="$uuid"/>
                </tradeHeader>
            </trade>
            <swap>
                <trader name="$trader" id="" />
            </swap>
        </clearingConfirmed>
        </tradeweb:FpML>
        </xml>
    ''')
    formatted_swap = swapxml_template.substitute(uuid=uuid, start_date=start_date, termination_date=termination_date, floating_rate=floating_rate,
                                                 spread=spread, fixed_rate=fixed_rate, fixed_payee=fixed_payee, float_payee=float_payee, trader=trader, transaction_time=transaction_time)
    return formatted_swap


def respond_denied(uuid, start_date, termination_date, floating_rate, spread, fixed_rate, fixed_payee, float_payee, trader, transaction_time):
    swapxml_template = Template(
        '''<tradeweb:FpML xmlns="http://www.fpml.org/FpML-5/confirmation" xmlns:tradeweb="" xmlns:dsig="http://www.w3.org/2000/09/xmldsig#">
        <clearingDenied fpmlVersion="5-0">
            <header>
                <!-- metadata on the message goes here. -->
            </header>
            <trade>
                <tradeHeader xsi:type="tradeweb:TWTradeHeader" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <execution>
                        <transact_time>$transaction_time</transact_time>
                    </execution>
                    <tradeId uuid="$uuid"/>
                </tradeHeader>
            </trade>
            <swap>
                <trader name="$trader" id="" />
            </swap>
        </clearingDenied>
        </tradeweb:FpML>
    ''')
    formatted_swap = swapxml_template.substitute(uuid=uuid, start_date=start_date, termination_date=termination_date, floating_rate=floating_rate,
                                                 spread=spread, fixed_rate=fixed_rate, fixed_payee=fixed_payee, float_payee=float_payee, trader=trader, transaction_time=transaction_time)
    return formatted_swap


def on_message(channel, method_frame, header_frame, body):
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    # print method_frame.delivery_tag
    # print body
    xmlresponse = read_and_respond_from_xml(body)
    channel.basic_publish(
        exchange='', routing_key=clearing_to_tw, body=xmlresponse)


clearingconf = ''
try:
    opts, args = getopt.getopt(sys.argv[1:], "hc:", ["clearingconf="])
except getopt.GetoptError:
    print 'clearing.py -c <clearingconf>'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        print 'clearing.py -c <clearingconf>'
        sys.exit()
    elif opt in ("-c", "--conf"):
        clearingconf = arg
if len(clearingconf) < 1:
    print 'Config file must be specified.'
    print 'clearing.py -c <clearingconf>'
    sys.exit()

parser = SafeConfigParser()
parser.read(clearingconf)
queuedata = dict(parser.items('queue'))
tw_to_clearing = str(queuedata.get('tw_to_clearing', 'tw_to_clearing'))
clearing_to_tw = str(queuedata.get('clearing_to_tw', 'clearing_to_tw'))

channel.queue_declare(queue=clearing_to_tw)
channel.queue_declare(queue=tw_to_clearing)

channel.basic_consume(on_message, tw_to_clearing)

try:
    channel.start_consuming()
except KeyboardInterrupt:
    channel.stop_consuming()
connection.close()
