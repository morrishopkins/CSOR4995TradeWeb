from flask import Flask, make_response, request, redirect, url_for, send_from_directory, abort, jsonify
import json
from apps.tradeweb import app, db, channel, get_pubswapqname, get_readswapqname
from apps.tradeweb._models.InterestRateSwap import InterestRateSwap as Swap
from apps.tradeweb._models.EOD import *
from datetime import datetime
from string import Template
import pika

@app.route('/swaps')
def swaps():
    ''' Make a call to db to retrieve all swaps. '''
    try:
        swaps = Swap.objects(cleared=True)
        resp = jsonify(data=swaps)
        return make_response(resp, 200)
    except:
        return "ERROR", 500

@app.route('/swapsdenied')
def swapsdenied():
    ''' Make a call to db to retrieve all swaps that were denied during clearing. '''
    try:
        swaps = Swap.objects(cleared=False)
        resp = jsonify(data=swaps)
        return make_response(resp, 200)
    except:
        return "ERROR", 500


@app.route('/swap', methods=['POST'])
def add_swap():
    print 'in the swap POST route'
    print request.form
    start_date = datetime.strptime(request.form['start_date'], '%m/%d/%Y')
    termination_date = datetime.strptime(request.form['termination_date'], '%m/%d/%Y')
    floating_rate = 0.24300 #request.form['floating_rate']
    spread = float(request.form['spread'])
    fixed_rate = float(request.form['fixed_rate'])
    fixed_payee = request.form['fixed_payee'].lower()
    float_payee = request.form['float_payee'].lower()
    trader = request.form['trader'].lower()
    transaction_time = datetime.now()

    print request.form['start_date'] + request.form['termination_date'] + str(floating_rate) + str(spread) + str(fixed_rate) + fixed_payee + float_payee + trader
    try:
        print 'about to create swap'
        newswap = Swap(start_date=start_date, termination_date=termination_date, floating_rate=floating_rate, spread=spread, fixed_rate=fixed_rate, fixed_payee=fixed_payee, float_payee=float_payee, trader=trader, transaction_time=transaction_time)
        xmlswap = newswap.save()
        print 'swap created.'
        x = get_swap_as_xml(newswap.uuid, start_date=start_date, termination_date=termination_date, floating_rate=floating_rate, spread=spread, fixed_rate=fixed_rate, fixed_payee=fixed_payee, float_payee=float_payee, trader=trader, transaction_time=transaction_time)
        print 'Converted to xml.'
        channel.basic_publish(exchange='', routing_key=get_pubswapqname(), body=x)
        return make_response('{"status": "200", "msg":"Swap sent out for clearing."}')
    except Exception as e:
        print e.__doc__
        print e.message
        return '{"msg":"Aborting due to error inserting offer into database."}', 500


@app.route('/swapspnl')
def swapspnl():
  ''' Returns pnl for all swaps '''
  return json.dumps(Swap.swap_pnl())


@app.route('/swapstraderpnl')
def swapstraderpnl():
  ''' Returns pnl for all swaps by trader '''
  return json.dumps(Swap.swap_pnl_by_trader())


# This route will prompt a file download with the csv lines
@app.route('/download_eod')
def download_eod():
    csv = EOD_Order()
    print csv
    response = make_response(csv)
    # This is the key: Set the right header for the response
    # to be downloaded, instead of just printed on the browser
    response.headers["Content-Disposition"] = "attachment; filename=order.csv"
    return response


# This route will prompt a file download with the csv lines
@app.route('/download_eod_swap')
def download_eod_swap():
    csv = EOD_Swap()
    print " swaps csv "
    print csv
    response = make_response(csv)
    # This is the key: Set the right header for the response
    # to be downloaded, instead of just printed on the browser
    response.headers["Content-Disposition"] = "attachment; filename=swap.csv"
    return response


def get_swap_as_xml(uuid, start_date, termination_date, floating_rate, spread, fixed_rate, fixed_payee, float_payee, trader, transaction_time):
    swapxml_template = Template(\
        '''<tradeweb:FpML xmlns="http://www.fpml.org/FpML-5/confirmation" xmlns:tradeweb="" xmlns:dsig="http://www.w3.org/2000/09/xmldsig#">
        <clearingRequested fpmlVersion="5-0">
            <header>
                <!-- metadata on the message goes here. -->
            </header>
            <trade>
                <tradeHeader xsi:type="tradeweb:TWTradeHeader" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <execution>
                        <transact_time>$transaction_time</transact_time>
                    </execution>
                    <tradeId uuid="$uuid"/>
                </tradeHeader>
            </trade>
            <swap>
                <swapStream id="floatLeg">
                    <payee name="$float_payee"/>
                    <rate id="libor" amnt="$floating_rate"/>
                </swapStream>
                <swapStream id="fixedLeg">
                    <payee name="$fixed_payee"/>
                    <rate id="" amnt="$fixed_rate"/>
                </swapStream>
                <spread amnt="$spread"/>
                <contract_dates>
                    <start_date>$start_date</start_date>
                    <termination_date>$termination_date</termination_date>
                </contract_dates>
                <trader name="$trader" id="" />
            </swap>
        </clearingRequested>
        </tradeweb:FpML>
    ''')
    formatted_swap = swapxml_template.substitute(uuid=uuid, start_date=start_date, termination_date=termination_date, floating_rate=floating_rate, spread=spread, fixed_rate=fixed_rate, fixed_payee=fixed_payee, float_payee=float_payee, trader=trader, transaction_time=transaction_time)
    return formatted_swap





