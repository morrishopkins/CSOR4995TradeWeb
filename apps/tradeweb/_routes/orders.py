from flask import Flask, make_response, request, redirect, url_for, send_from_directory, abort, jsonify
import json
from apps.tradeweb import app, db
from apps.tradeweb._models.Order import Order
import pika


@app.route('/orders')
def orders():
  ''' Make a call to db to retrieve all orders. '''
  try:
    orders = Order.objects.all()
    resp = jsonify(data=orders)
    return make_response(resp, 200)
  except:
    return "ERROR", 500

@app.route('/orderpositions')
def orderpositions():
  return json.dumps(Order.calculate_positions_by_expiry())

@app.route('/orderspnl')
def orderspnl():
  return json.dumps(Order.calculate_pnl())

@app.route('/traderpnl')
def pnl_novel():
  return json.dumps(Order.calculate_traderpnl())


# def add_order(channel, method_frame, header_frame, body):
#   print 'in the add_order function'
#   print method_frame.delivery_tag
#   print body
#   parsed_json = json.loads(body)

#   contract_exp = time.strptime(parsed_json['contract_exp'].encode("utf-8"), "%y-%m-%d")
#   order_id = parsed_json['order_id']
#   order_type = parsed_json['order_type'].encode("utf-8")
#   price = parsed_json['price']
#   side = parsed_json['side'].encode("utf-8")
#   symbol = parsed_json['symbol'].encode("utf-8")
#   trade_date = time.strptime(parsed_json['trade_date'].encode("utf-8"), "%y-%m-%d")
#   trader = parsed_json['trader'].encode("utf-8")
#   transaction_time = parsed_json['transaction_time'].encode("utf-8")

#   try:
#     neworder = Order(contract_exp=contract_exp, offer_id=offer_id, order_id=order_id, order_type=order_type, price=price, side=side, symbol=symbol, trade_date=trade_date, trader=trader, transaction_time=transaction_time)
#     neworder.save()
#     # neworderstring = neworder.to_json()
#   except:
#     return '{"msg":"Aborting due to error inserting order into database."}', 500


# ###############################

# # Step #3
# def on_open(connection):
#   connection.channel(on_channel_open)

# # Step #4
# def on_channel_open(channel):
#   channel.queue_declare(queue='neworder')
#   channel.basic_consume(add_order, 'neworder')
#   channel.start_consuming()




# #Initialize non blocking consumer
# parameters = pika.ConnectionParameters('localhost')
# connection = pika.SelectConnection(parameters=parameters,
#                                    on_open_callback=on_open)

# try:
#   # Step #2 - Block on the IOLoop
#   connection.ioloop.start()

# # Catch a Keyboard Interrupt to make sure that the connection is closed cleanly
# except KeyboardInterrupt:
#   # Gracefully close the connection
#   connection.close()

#   # Start the IOLoop again so Pika can communicate, it will stop on its own when the connection is closed
#   connection.ioloop.start()








