from flask import Flask, make_response, request, redirect, url_for, send_from_directory, abort, jsonify
import ystockquote
from datetime import datetime
import json
from bson import json_util
from apps.tradeweb import app, db, tu
from apps.tradeweb._models.Offer import Offer
import pika

tu=tu.TradeDateUtil()
symbol_cache = ['LN','NN','NG','NP','HH','ON','HP','PD','HB','TC','PH','NR','TR','5Z','NL','GL','PG','IN','NH','9F','PW','NW','CI','G4','6J','NX','NK','CZ','NJ','ZR','E7','TME','SZ','IX','PM','DSF','NE','PJ','FP','NF','TDE','TZ6','NZ','XJ','Q1','PF','TX','8X','PE','G3','CFS','NM','QG','6Z','DVS','ZN','8Z','G2','MFS','CW','PK','G6','ND','XR','G7','M8','B4','WFS','PFS','NME','IV','XN','Z5P','U07','B2','ZTS','NS','DVA','DAC','U12','IS','LAI','XZ4','XNC','XZ','5F','T4','SK','GME','N5','U30','TZ','N4','SS','Q2','DUE','V8','U23','PU','VX','U05','IJ','LN3','X5','L2','DI','9A','ON1','5E','U14','IU','HHT','TZI','ZNP','KD','OX','XSC','Y7','5M','T7','SM','DNG','HBI','G5','SU','Q9','IPE','K7','U25','XH','IL','LN5','VNQ','Z8','GND','DW','C4','ON3','5H','U16','IW','NNT','TZ5','DML','ZNT','IC','PC','JKM','XAC','SX','SF','GRE','M3','U27','U09','IP','DPP','VNA','ZTA','IM','SN','J6','U8','U02','IF','C9','ON5','W5','U18','IY','NB','DAL','MNG','XZ3','NV','XGC','XX','4D','T2','SJ','GNE','C7','M7','U29','XQ','PY','U11','IR','NFS','DCE','DNP','SR','UKE','T9','U22','VS','U04','II','IA','LN2','X4','U20','K8','A1','5D','U13','IT','TE','XZ6','OI','XKC','Y6','5I','T6','SL','UKD','N7','U31','HHS','ST','Q4','NVT','K6','U24','W9','U06','IK','LN4','Y9','GRD','DT','C3','ON2','5G','U15','8A','NGT','DFE','ZZ6','IB','PB','Y8','5O','T8','XIC','SV','S9','DPE','L4','U26','4W','U08','CFP','VNS','NDE','M6','U01','E2','C8','ON4','5N','U17','8E','TZS','DAR','IE','NQ','XZ1','XFC','5K','SY','SH','GGE','M4','U28','XO','6I','U10','IQ','TEP','SGW','IZ','SQ','PX','ASE','J8','U21','VI','U03','IH','LN1','X2','U19','J3']

# Pika connection to queue
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()
channel.queue_declare(queue='newoffer')


@app.route('/offer')
def offers():
  ''' Needs to make a call to db to retrieve all offers. '''
  try:
    offers = Offer.objects.all()
    resp = jsonify(data=offers)
    return make_response(resp, 200)
  except:
    return "ERROR", 500

@app.route('/offer', methods=['POST'])
def add_offer():
  print 'in the offer POST route'
  print request.form
  symbol = request.form['symbol']
  contract_exp = datetime.strptime(request.form['contract_exp'], '%m/%d/%Y')
  contract_exp = tu.get_endofmonth_business_day(contract_exp, 3)
  print "Contract Exp."
  print contract_exp
  side = request.form['side']
  trader = request.form['trader'].lower()
  # Default values for fields that are not always supplied.
  try:
    price = request.form['price']
  except KeyError as ke:
    price = -999

  lots = request.form['lots']
  offer_type = request.form['offer_type']
  offer_date = datetime.now
  transaction_time = datetime.now
  try:
    newoffer = Offer(symbol=symbol, contract_exp=contract_exp, side=side, trader=trader, price=price, lots=lots, offer_date=offer_date, offer_type=offer_type, transaction_time=transaction_time)
    newoffer.save()
    newofferstring = newoffer.to_json()
    channel.basic_publish(exchange='', routing_key='newoffer', body=newofferstring)
    print 'Published to rabbitmq.'
    return make_response('{"status": "200", "msg":"Offer saved successfully."}')
  except:
    return '{"msg":"Aborting due to error inserting offer into database."}', 500


@app.route('/symbols')
def symbols():
  ''' Needs to make a call to db to retrieve all offers. '''
  return json.dumps(symbol_cache)

@app.route('/positions')
def positions():
  return json.dumps(Offer.calculate_position_by_sym())

@app.route('/positions_trader')
def positions_trader():
  return json.dumps(Offer.calculate_positions_by_trader())

@app.route('/positions_expiry')
def positions_expiry():
  return json.dumps(Offer.calculate_positions_by_expiry())




