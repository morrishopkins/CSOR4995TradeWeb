from flask import Flask
from apps.tradeweb import app
import ystockquote
import json


@app.route('/mktdata/all')
def marketdata():
  quotes = ystockquote.get_all(['^IXIC', '^NYA', '^GSPC', '^FTSE', '^N225', '^HSI', '^FCHI', '^GDAXI', 'AAPL', 'MSFT', 'TSLA', 'EURUSD=X', 'GBPUSD=X', 'BZX15.NYM', 'GCV15.CMX', 'FCV15.CME'])
  # print quotes
  return json.dumps(quotes)
