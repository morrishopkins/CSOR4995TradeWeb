Ext.define('TradeStorm.Application', {
    extend: 'Ext.app.Application',

    name: 'TradeStorm',
    
    stores: [
        'NavigationTree'
    ],


    defaultToken : 'offerblotter',

    //controllers: [
        // TODO - Add Global View Controllers here
    //],

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
