/**
 * List of trade position aggregated by symbol.
 */
Ext.define('TradeStorm.view.swapblotter.TraderPnlList', {
    extend: 'Ext.grid.Panel',
    requires: [
    'TradeStorm.store.swaps.TraderPnL',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'swap_trader_pnl',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'swaptraderpnl'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Swap Trader PnL Report',
                fileName: 'swap_trader_pnl.xml'
            });
        }
    }],
    columns: [{
        text: 'Trader',
        dataIndex: 'trader',
        flex: 2
    },
    {
        text: 'PnL',
        dataIndex: 'pnl',
        flex: 6
    }]
});