/**
 * This view is a list of all swaps entered.
 */
Ext.define('TradeStorm.view.swapblotter.List', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.swaps.Swaps',
        'TradeStorm.view.swapblotter.UploadWindow',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'swaplist',
    layout: 'fit',
    ui: 'light-panel',
    border: false,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'swaps'
    },
    title: 'Swap Blotter',
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Swap Report',
                fileName: 'swaps_report.xml'
            });
        }
    }, {
        type: 'plus',
        tooltip: 'Add',
        callback: function(panel, tool, event) {
            Ext.create('swapuploadWindow').show();
        }
    }
    ],


    columns: [
        {
            text: 'Start Date',
            dataIndex: 'start_date',
            flex: 1
        },
        {
            // This is maturity date. User enters month and year
            // and the date is set to given month 3 business days
            // before end of month.
            text: 'Termination Date',
            dataIndex: 'termination_date',
            flex: 1
        },
        {
            text: 'Floating Rate (FR)', // This is a type (e.g. 'libor') (dropdown)
            dataIndex: 'floating_rate',
            flex: 1
        },
        {
            text: 'Spread', // floating rate spread percentage 0.0 - 4.0
            dataIndex: 'spread',
            flex: 1
        },
        {
            text: 'Fixed Rate', //percentage
            dataIndex: 'fixed_rate',
            flex: 1
        },
        {
            text: 'Fixed Payee', // dropdown with trader, cme, or lch
            dataIndex: 'fixed_payee',
            flex: 1
        },
        {
            text: 'Float Payee', // dropdown with trader, cme, or lch (if fixed is clearing house, then float is trader and vice versa)
            dataIndex: 'float_payee',
            flex: 1
        },
        {
            text: 'Trader',
            dataIndex: 'trader',
            flex: 1
        },
        {
            text: 'Transaction Time',
            dataIndex: 'transaction_time',
            flex: 1
        }
    ]
});


