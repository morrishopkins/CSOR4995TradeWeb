/**
 * Panel holding trade blotter list of all trades.
 */
Ext.define('TradeStorm.view.swapblotter.SwapBlotter', {
    extend: 'Ext.container.Container',
    xtype: 'swapblottercontainer',
    requires: [
        'TradeStorm.view.swapblotter.List',
        'TradeStorm.view.swapblotter.SwapBlotterController'
    ],
    controller: 'swapblotter',
    layout: 'fit',
    bodyPadding: 10,
    items: [
        {
            xtype: 'swaplist'
        }
    ]
});
