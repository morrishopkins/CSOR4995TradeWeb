Ext.create('Ext.data.Store', {
    alias: 'store.floatrate',
    storeId: 'floatrate',
    fields: ['name'],
    data: [{
        "name": "libor"
    }]
});

Ext.create('Ext.data.Store', {
    alias: 'store.payee',
    storeId: 'payee',
    fields: ['name'],
    data: [{
        "name": "trader"
    }, {
        "name": "cme"
    }, {
        "name": "lch"
    }]
});


Ext.define('TradeStorm.view.swapblotter.UploadWindow', {
    extend: 'Ext.window.Window',
    requires: [
        'TradeStorm.view.swapblotter.UploadWindowController'
    ],
    xtype: 'swapuploadwindow',
    title: 'Post new swap',
    alias: 'swapuploadWindow',
    controller: 'swapuploadwindow',
    height: 450,
    width: 355,
    resizable: false,
    bodyPadding: 0,
    items: [{
        xtype: 'form',
        bodyPadding: 10,
        height: 400,
        url: '/swap',
        width: 350,
        border: false,
        header: false,
        defaults: {
            anchor: '100%',
            labelWidth: 100
        },
        items: [{
                xtype: 'datefield',
                name: 'start_date',
                fieldLabel: 'Start Date',
                allowBlank: false
            }, {
                xtype: 'datefield',
                name: 'termination_date',
                fieldLabel: 'Term. Date',
                allowBlank: false
            }, {
                xtype: 'combo',
                name: 'floating_rate',
                inputId: 'floating_rate',
                fieldLabel: 'Floating Rate',
                store: 'floatrate',
                queryMode: 'local',
                displayField: 'name',
                valueField: 'name',
            }, {
                xtype: 'numberfield',
                fieldLabel: 'Spread',
                name: 'spread',
                value: 0.00,
                minValue: 0.00,
                maxValue: 4.00,
                allowDecimals: true,
                decimalPrecision: 3,
                step: 0.050,
                disabled: false,
                reference: 'fr_spread'
            }, {
                xtype: 'numberfield',
                fieldLabel: 'Fixed Rate',
                name: 'fixed_rate',
                value: 0.00,
                minValue: 0.00,
                maxValue: 4.00,
                allowDecimals: true,
                decimalPrecision: 3,
                step: 0.050,
                disabled: false,
                reference: 'fixed_rate'
            }, {
                xtype: 'combo',
                name: 'fixed_payee',
                inputId: 'fixed_payee',
                fieldLabel: 'Fixed Payee',
                store: 'payee',
                queryMode: 'local',
                displayField: 'name',
                valueField: 'name',
            },

            {
                xtype: 'combo',
                name: 'float_payee',
                inputId: 'float_payee',
                fieldLabel: 'Float Payee',
                store: 'payee',
                queryMode: 'local',
                displayField: 'name',
                valueField: 'name',
            },

            {
                xtype: 'textfield',
                allowBlank: false,
                fieldLabel: 'Trader',
                name: 'trader',
                emptyText: 'trader'
            }
        ],
        buttons: [{
            text: 'Post Swap',
            formBind: true,
            handler: 'postTradeClick'
        }]
    }]
});