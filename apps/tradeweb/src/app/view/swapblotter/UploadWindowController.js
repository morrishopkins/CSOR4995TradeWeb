Ext.define('TradeStorm.view.swapblotter.UploadWindowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.swapuploadwindow',
    postTradeClick: function() {
        var viewPanel = this.view;
        var upForm = viewPanel.items.items[0].getForm();
        formvals = upForm.getValues();
        if (upForm.isValid()) {
            Ext.MessageBox.show({
                title: 'Confirm Order',
                msg: 'Please confirm the following swap order:<br><br>' +
                'Start date: ' + formvals['start_date'] + ',<br>Termination Date: ' + formvals['termination_date'] +
                ',<br>Spread: ' + formvals['spread'] + ',<br>Floating Rate: ' + formvals['floating_rate']  + ',<br>Fixed Rate: ' + formvals['fixed_rate'] +
                ',<br>Floating Payee: ' + formvals['floating_payee']  + ',<br>Fixed Payee: ' + formvals['fixed_payee'] + ',<br>Trader: ' + formvals['trader'],
                buttons: Ext.MessageBox.OKCANCEL,
                icon: Ext.MessageBox.WARNING,
                fn: function(btn){
                    if(btn == 'ok'){
                        upForm.submit({
                            success: function(upForm, action) {
                                console.log(upForm);
                                console.log(action);
                                Ext.Msg.alert('Success', action.result.msg);
                            },
                            failure: function(upForm, action) {
                                if(action.result.status == '200') {
                                    viewPanel.close();
                                    var swapstore = Ext.data.StoreManager.get("swaps");
                                    swapstore.reload();

                                } else {
                                    Ext.Msg.alert('Failed', action.result.msg);
                                }

                            }
                        });
                    } else {
                        return;
                    }
                }
            });
        }
    },
    limitSelected: function( rdio, newValue, oldValue, eOpts ){
        // field = this.lookupReference('limit_price');
        // field.enable();
    },

    peggedSelected: function( rdio, newValue, oldValue, eOpts ){
        // field = this.lookupReference('limit_price');
        // field.disable();
    }
});