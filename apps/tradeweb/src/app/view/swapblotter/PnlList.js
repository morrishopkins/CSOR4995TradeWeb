/**
 * List of trade position aggregated by symbol.
 */
Ext.define('TradeStorm.view.swapblotter.PnlList', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.swaps.PnL',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'swappnl',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'swappnl'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Swap PnL Report',
                fileName: 'swappnl.xml'
            });
        }
    }],
    columns: [
     {
            text: 'Start Date',
            dataIndex: 'start_date',
            flex: 1
        },
        {
            // This is maturity date. User enters month and year
            // and the date is set to given month 3 business days
            // before end of month.
            text: 'Termination Date',
            dataIndex: 'termination_date',
            flex: 1
        },
        {
            text: 'Floating Rate (FR)', // This is a type (e.g. 'libor') (dropdown)
            dataIndex: 'floating_rate',
            flex: 1
        },
        {
            text: 'Spread', // floating rate spread percentage 0.0 - 4.0
            dataIndex: 'spread',
            flex: 1
        },
        {
            text: 'Fixed Rate', //percentage
            dataIndex: 'fixed_rate',
            flex: 1
        },
        {
            text: 'Payed By',
            dataIndex: 'pay_receive',
            flex: 1
        },
        {
            text: 'Trader',
            dataIndex: 'trader',
            flex: 1
        },
        {
            text: 'PnL',
            dataIndex: 'pnl',
            flex: 1
        }
    ]
});

