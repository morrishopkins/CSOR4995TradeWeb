/**
 * Panel holding all swap aggregate info.
 */
Ext.define('TradeStorm.view.swapblotter.SwapAggregates', {
    extend: 'Ext.container.Container',
    xtype: 'swapaggregatescontainer',
    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Ext.tab.Panel',
        'TradeStorm.view.swapblotter.PnlList',
        'TradeStorm.view.swapblotter.TraderPnlList',
        'TradeStorm.view.swapblotter.SwapBlotterController',
        'TradeStorm.view.swapblotter.ClearingsDenied'
    ],
    controller: 'swapblotter',
    layout: 'fit',
    items: [
        {xtype: 'tabpanel',
         items: [
            {
                xtype: 'panel',
                title: 'Swaps PnL',
                items: [{xtype: 'swappnl'}]
            }, {
                xtype: 'panel',
                title: 'Swaps PnL by Trader',
                items: [{xtype: 'swap_trader_pnl'}]
            }, {
                xtype: 'panel',
                title: 'Swaps Denied at Clearing',
                items: [{xtype: 'swapsdenied'}]
            }
         ]
        }

    ]
});
