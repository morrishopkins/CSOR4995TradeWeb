Ext.define('TradeStorm.view.swapblotter.SwapBlotterController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.swapblotter',

    // This can be implemented later to get more trade 
    // information by clicking on a trade in the swapblotter.
    onItemSelected: function() {

    }
});