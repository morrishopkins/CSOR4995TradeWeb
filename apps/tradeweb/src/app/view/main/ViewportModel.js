/**
 * used to track history.
 */
Ext.define('TradeStorm.view.main.ViewportModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.mainviewport',

    data: {
        currentView: null
    }
});
