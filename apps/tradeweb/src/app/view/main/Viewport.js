/**
 * Provides the navigation tree and paneling for the app.
 */
Ext.define('TradeStorm.view.main.Viewport', {
    extend: 'Ext.container.Viewport',
    xtype: 'mainviewport',

    requires: [
        'Ext.list.Tree',
        'TradeStorm.view.main.ViewportController',
        'TradeStorm.view.main.MainContainerWrap',
        'TradeStorm.view.main.ViewportModel'
    ],

    controller: 'mainviewport',
    viewModel: {
        type: 'mainviewport'
    },

    itemId: 'mainView',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    listeners: {
        render: 'onMainViewRender'
    },

    items: [
        {
            xtype: 'toolbar',
            height: 64,
            ui: 'mainbar',
            padding: '6 0',
            items: [
                {
                    ui: 'peak-logo',
                    itemId: 'main-navigation-btn',
                    reference: 'peakLogo',
                    icon: 'css/images/logo48.png',
                    text: 'TradeWeb',
                    width: 245,
                    padding: 0,
                    handler: 'onToggleNavigationSize',
                    scale: 'large',
                    border: 0

                },
                {
                    xtype: 'tbspacer',
                    flex: 1
                },
                {
                    xtype: 'button',
                    text: 'Run EOD',
                    handler: 'runeod'
                }
            ]
        },
        {
            xtype: 'maincontainerwrap',
            itemId: 'main-view-detail-wrap',
            reference: 'mainContainerWrap',
            flex: 1,
            items: [
                {
                    xtype: 'treelist',
                    reference: 'navigationTreeList',
                    itemId: 'navigationTreeList',
                    store: 'NavigationTree',
                    style: 'border-right: 1px solid rgba(0,0,0,0.2);',
                    width: 250,
                    expanderFirst: false,
                    expanderOnly: false,
                    listeners: {
                        selectionchange: 'onNavigationTreeSelectionChange'
                    }
                },
                {
                    xtype: 'container',
                    flex: 1,
                    reference: 'mainCardPanel',
                    itemId: 'contentPanel',
                    layout: {
                        type: 'card',
                        anchor: '100%'
                    }
                }
            ]
        }
    ]
});
