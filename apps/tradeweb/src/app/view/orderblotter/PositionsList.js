/**
 * List of trade position aggregated by symbol.
 */
Ext.define('TradeStorm.view.orderblotter.PositionsList', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.orders.Positions',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'orderpositionslist',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'orderpositions'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Position Price Aggregate Report',
                fileName: 'positions.xml'
            });
        }
    }],
    columns: [{
        text: 'Symbol',
        dataIndex: 'symbol',
        flex: 2
    },
    {
        text: 'Exp. Date',
        dataIndex: 'contract_exp',
        flex: 1
    },
    {
        text: 'Lots',
        dataIndex: 'lots',
        flex: 1
    },
    {
        text: 'Price',
        dataIndex: 'price',
        flex: 6
    }]
});