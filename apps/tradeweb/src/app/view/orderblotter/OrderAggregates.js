/**
 * Panel holding all aggregate info. 
 */
Ext.define('TradeStorm.view.orderblotter.OrderAggregates', {
    extend: 'Ext.container.Container',
    xtype: 'orderaggregatescontainer',
    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Ext.tab.Panel',
        'TradeStorm.view.orderblotter.PositionsList',
        'TradeStorm.view.orderblotter.TraderPnlList',
        'TradeStorm.view.orderblotter.PnlList',
        'TradeStorm.view.orderblotter.OrderBlotterController'
    ],
    controller: 'orderblotter',
    layout: 'fit',
    items: [
        {xtype: 'tabpanel',
         items: [
            {
                xtype: 'panel',
                title: 'Orders by Price',
                items: [{xtype: 'orderpositionslist'}]
            },
            {
                xtype: 'panel',
                title: 'PnL Report',
                items: [{xtype: 'orderpnllist'}]
            },
            {
                xtype: 'panel',
                title: 'Trader PnL Report',
                items: [{xtype: 'traderpnllist'}]
            }
         ]
        }

    ]
});
