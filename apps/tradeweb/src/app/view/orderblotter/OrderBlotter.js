/**
 * Panel holding trade blotter list of all trades.
 */
Ext.define('TradeStorm.view.orderblotter.OrderBlotter', {
    extend: 'Ext.container.Container',
    xtype: 'orderblottercontainer',
    requires: [
        'TradeStorm.view.orderblotter.List',
        'Ext.ux.layout.ResponsiveColumn',
        'TradeStorm.view.orderblotter.OrderBlotterController'
    ],
    controller: 'orderblotter',
    layout: 'fit',
    bodyPadding: 10,
    items: [
        {
            xtype: 'orderlist'
        }
    ]
});
