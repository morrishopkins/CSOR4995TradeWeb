/**
 * List of trade position aggregated by symbol.
 */
Ext.define('TradeStorm.view.orderblotter.PnlList', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.orders.PnL',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'orderpnllist',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'orderpnl'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Position PnL Report',
                fileName: 'pnl.xml'
            });
        }
    }],
    columns: [{
        text: 'Symbol',
        dataIndex: 'symbol',
        flex: 2
    },
    {
        text: 'Exp. Date',
        dataIndex: 'contract_exp',
        flex: 1
    },
    {
        text: 'PnL',
        dataIndex: 'pnl',
        flex: 6
    }]
});