/**
 * List of trade position aggregated by symbol.
 */
Ext.define('TradeStorm.view.orderblotter.TraderPnlList', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.orders.TraderPnL',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'traderpnllist',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'traderpnl'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Trader PnL Report',
                fileName: 'pnl.xml'
            });
        }
    }],
    columns: [{
        text: 'Trader',
        dataIndex: 'trader',
        flex: 2
    },
    {
        text: 'PnL',
        dataIndex: 'pnl',
        flex: 6
    }]
});