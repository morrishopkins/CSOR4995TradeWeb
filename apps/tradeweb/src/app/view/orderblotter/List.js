/**
 * This view is a list of all offers entered.
 */
Ext.define('TradeStorm.view.orderblotter.List', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.orders.Orders',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'orderlist',
    layout: 'fit',
    ui: 'light-panel',
    border: false,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'orders'
    },


    title: 'Order Blotter',
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Order Report',
                fileName: 'myExport.xml'
            });
        }
    }],

    columns: [{
        text: 'Symbol',
        dataIndex: 'symbol',
        flex: 1
    },
    {
        text: 'Order ID',
        dataIndex: 'order_id',
        flex: 1
    },
    {
        text: 'Order Type',
        dataIndex: 'order_type',
        flex: 1
    },
    {
        text: 'Trade Date',
        dataIndex: 'trade_date',
        flex: 1
    },
    {
        text: 'Transaction Time',
        dataIndex: 'transaction_time',
        flex: 1
    },

    {
        text: 'Expr. Date',
        dataIndex: 'contract_exp',
        flex: 1
    }, {
        text: 'Lots',
        dataIndex: 'lots',
        flex: 1
    }, {
        text: 'Price',
        dataIndex: 'price',
        flex: 1
    },
    {
        text: 'Side',
        dataIndex: 'side',
        flex: 1
    }, {
        text: 'Trader',
        dataIndex: 'trader',
        flex: 1
    }],
    listeners: {
        select: 'onItemSelected'
    }
});