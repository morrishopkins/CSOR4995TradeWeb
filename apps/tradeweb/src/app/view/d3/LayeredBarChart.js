Ext.define('TradeStorm.view.d3.LayeredBarChart', {
    extend: 'Ext.container.Container',
    xtype: 'd3barchart',
    layout: 'fit',
    style: "width: 100%; height: 100%;",
    layout: {
        type: 'fit',
        align: 'stretch'
    },

    constructor: function(config) {
        this.chartConfig = config.chartConfig || {
            margin: {
                top: 40,
                right: 10,
                bottom: 20,
                left: 10
            },
            layerCount: 4,
            layerSamples: 55
        };

        this.selectPanel = Ext.create('Ext.form.Panel', {
            header: false,
            width: 200,
            maxWidth: 200,
            height: 45,
            maxHeight: 45,
            bodyPadding: 10,
            items: [{
                xtype: 'radiogroup',
                columns: 2,
                items: [{
                    boxLabel: 'Grouped',
                    name: 'rb',
                    inputValue: 'grouped',
                    listeners: {
                        'change': this.change
                    }
                }, {
                    boxLabel: 'Stacked',
                    name: 'rb',
                    inputValue: 'stacked',
                    checked: true,
                    listeners: {
                        'change': this.change
                    }
                }]
            }]
        });




        Ext.apply(this, config);
        this.callParent([config]);
    },

    listeners: {
        'afterrender': function() {
            this.add(this.selectPanel);
            this.renderD3(this, this.getId());
        }
    },

    renderD3: function(scope, d3Canvas) {
        me = scope;
        var n = this.chartConfig.layerCount, // number of layers
            m = this.chartConfig.layerSamples, // number of samples per layer
            stack = d3.layout.stack();
        me.n = n;
        me.m = m;
        me.layers = stack(d3.range(n).map(function() {
            return me.bumpLayer(m, .1);
        }));
        me.yGroupMax = d3.max(me.layers, function(layer) {
            return d3.max(layer, function(d) {
                return d.y;
            });
        });
        me.yStackMax = d3.max(me.layers, function(layer) {
            return d3.max(layer, function(d) {
                return d.y0 + d.y;
            });
        });

        var margin = this.chartConfig.margin;
        me.width = me.getWidth() - margin.left - margin.right;
        me.height = me.getHeight() - margin.top - margin.bottom;

        var x = d3.scale.ordinal()
            .domain(d3.range(m))
            .rangeRoundBands([0, me.width], .08);

        var y = d3.scale.linear()
            .domain([0, me.yStackMax])
            .range([me.height, 0]);

        me.x = x;
        me.y = y;

        var color = d3.scale.linear()
            .domain([0, n - 1])
            .range(["#aad", "#556"]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .tickSize(0)
            .tickPadding(6)
            .orient("bottom");

        var svg = d3.select("#" + d3Canvas).append("svg")
            .attr("width", me.width + margin.left + margin.right)
            .attr("height", me.height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        var layer = svg.selectAll(".layer")
            .data(me.layers)
            .enter().append("g")
            .attr("class", "layer")
            .style("fill", function(d, i) {
                return color(i);
            });

        me.rect = layer.selectAll("rect")
            .data(function(d) {
                return d;
            })
            .enter().append("rect")
            .attr("x", function(d) {
                return x(d.x);
            })
            .attr("y", me.height)
            .attr("width", x.rangeBand())
            .attr("height", 0);

        me.rect.transition()
            .delay(function(d, i) {
                return i * 10;
            })
            .attr("y", function(d) {
                return y(d.y0 + d.y);
            })
            .attr("height", function(d) {
                return y(d.y0) - y(d.y0 + d.y);
            });

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + me.height + ")")
            .call(xAxis);
    },
    change: function() {
        if (this.inputValue === "grouped") me.transitionStacked();
        else me.transitionGrouped();
    },

    transitionGrouped: function() {
        me.y.domain([0, me.yGroupMax]);

        me.rect.transition()
            .duration(500)
            .delay(function(d, i) {
                return i * 10;
            })
            .attr("x", function(d, i, j) {
                return me.x(d.x) + me.x.rangeBand() / me.n * j;
            })
            .attr("width", me.x.rangeBand() / me.n)
            .transition()
            .attr("y", function(d) {
                return me.y(d.y);
            })
            .attr("height", function(d) {
                return me.height - me.y(d.y);
            });
    },

    transitionStacked: function() {
        me.y.domain([0, me.yStackMax]);

        me.rect.transition()
            .duration(500)
            .delay(function(d, i) {
                return i * 10;
            })
            .attr("y", function(d) {
                return me.y(d.y0 + d.y);
            })
            .attr("height", function(d) {
                return me.y(d.y0) - me.y(d.y0 + d.y);
            })
            .transition()
            .attr("x", function(d) {
                return me.x(d.x);
            })
            .attr("width", me.x.rangeBand());
    },

    bumpLayer: function(n, o) {

        function bump(a) {
            var x = 1 / (.1 + Math.random()),
                y = 2 * Math.random() - .5,
                z = 10 / (.1 + Math.random());
            for (var i = 0; i < n; i++) {
                var w = (i / n - y) * z;
                a[i] += x * Math.exp(-w * w);
            }
        }

        var a = [],
            i;
        for (i = 0; i < n; ++i) a[i] = o + o * Math.random();
        for (i = 0; i < 5; ++i) bump(a);
        return a.map(function(d, i) {
            return {
                x: i,
                y: Math.max(0, d)
            };
        });
    }






});