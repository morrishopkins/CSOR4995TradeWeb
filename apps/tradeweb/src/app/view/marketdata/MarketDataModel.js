Ext.define('TradeStorm.view.marketdata.MarketDataModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.marketdata',

    requires: [
        'Ext.data.Store',
        'Ext.data.field.Integer',
        'Ext.data.field.String',
        'Ext.data.field.Boolean'
    ],

    stores: {
        'all': {
            autoLoad: true,
            model: 'TradeStorm.model.MarketData',
            proxy: {
                 type: 'ajax',
                 url: '/mktdata/all',
                 reader: {
                     type: 'json'
                 }
             }

        }
    }
});

