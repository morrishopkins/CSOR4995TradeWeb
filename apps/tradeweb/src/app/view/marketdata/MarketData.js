/**
 * Outside of the scope of Chekpoint 1.
 * This is a view showing a bunch of boxes with live stock information.
 */
Ext.define('TradeStorm.view.marketdata.MarketData', {
    extend: 'Ext.container.Container',
    requires: [
        'Ext.ux.layout.ResponsiveColumn'
    ],
    id: 'marketdata',
    controller: 'marketdata',
    layout: 'responsivecolumn',
    viewModel: 'marketdata',
    defaults: {
        xtype: 'panel',
        height: 115,
        width: 150,
        bodyPadding: 15,
        border: false,
        header: false,
        ui: 'marketdata'
    },

    //really hacky right now but it works.
    // Need to figure out a way to move the initialization
    // of items to constructor, and loop over store.
    items: [
        // Ext.create('Ext.grid.Panel', {
        //     title: 'News',
        //     store: Ext.StoreManager.lookup('news'),
        //     columns: [
        //         { text: 'Symbol', dataIndex: 'name' },
        //         { text: 'Headline', dataIndex: 'email', flex: 1 }
        //     ],
        //     height: 385,
        //     width: 490,
        //     renderTo: Ext.getBody()
        // }),
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.0.color}; padding-top: 10px;">{all.data.items.0.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.0.last_trade_price} <span>&nbsp;{all.data.items.0.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.0.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.1.color}; padding-top: 10px;">{all.data.items.1.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.1.last_trade_price} <span>&nbsp;{all.data.items.1.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.1.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.2.color}; padding-top: 10px;">{all.data.items.2.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.2.last_trade_price} <span>&nbsp;{all.data.items.2.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.2.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.3.color}; padding-top: 10px;">{all.data.items.3.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.3.last_trade_price} <span>&nbsp;{all.data.items.3.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.3.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.4.color}; padding-top: 10px;">{all.data.items.4.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.4.last_trade_price} <span>&nbsp;{all.data.items.4.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.4.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.5.color}; padding-top: 10px;">{all.data.items.5.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.5.last_trade_price} <span>&nbsp;{all.data.items.5.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.5.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.6.color}; padding-top: 10px;">{all.data.items.6.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.6.last_trade_price} <span>&nbsp;{all.data.items.6.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 13px;">{all.data.items.6.name}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.7.color}; padding-top: 10px;">{all.data.items.7.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.7.last_trade_price} <span>&nbsp;{all.data.items.7.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.7.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.8.color}; padding-top: 10px;">{all.data.items.8.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.8.last_trade_price} <span>&nbsp;{all.data.items.8.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.8.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.9.color}; padding-top: 10px;">{all.data.items.9.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.9.last_trade_price} <span>&nbsp;{all.data.items.9.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.9.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.10.color}; padding-top: 10px;">{all.data.items.10.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.10.last_trade_price} <span>&nbsp;{all.data.items.10.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.10.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.11.color}; padding-top: 10px;">{all.data.items.11.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.11.last_trade_price} <span>&nbsp;{all.data.items.11.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.11.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.12.color}; padding-top: 10px;">{all.data.items.12.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.12.last_trade_price} <span>&nbsp;{all.data.items.12.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.12.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.13.color}; padding-top: 10px;">{all.data.items.13.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.13.last_trade_price} <span>&nbsp;{all.data.items.13.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.13.symbol}</div>'
            }
        },
        {
            bind: {
                html: '<div style="font-size: 30px; color: {all.data.items.14.color}; padding-top: 10px;">{all.data.items.14.change}</div>' +
                      '<div style="padding-top:15px;">{all.data.items.14.last_trade_price} <span>&nbsp;{all.data.items.14.change_percent}</span></div>' +
                      '<div style="padding-top:0px; font-size: 18px;">{all.data.items.14.symbol}</div>'
            }
        }



    ]
});


