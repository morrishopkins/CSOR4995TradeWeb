Ext.define('TradeStorm.view.marketdata.MarketDataController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.marketdata',

    requires: [
        'Ext.util.TaskRunner'
    ]
});
