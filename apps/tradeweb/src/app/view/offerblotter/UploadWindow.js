Ext.define('TradeStorm.view.offerblotter.UploadWindow', {
    extend: 'Ext.window.Window',
    requires: ['TradeStorm.store.Symbols',
        'TradeStorm.view.offerblotter.UploadWindowController'
    ],
    xtype: 'uploadwindow',
    title: 'Post new trade',
    alias: 'uploadWindow',
    controller: 'uploadwindow',
    height: 430,
    width: 355,
    resizable: false,
    bodyPadding: 0,
    items: [{
        xtype: 'form',
        bodyPadding: 10,
        height: 380,
        url: '/offer',
        width: 350,
        border: false,
        header: false,
        defaults: {
            anchor: '100%',
            labelWidth: 100
        },
        items: [{
            xtype: 'combobox',
            name: 'symbol',
            fieldLabel: 'Symbol',
            queryMode: 'local',
            displayField: 'symbol',
            valueField: 'symbol',
            anchor: '-15',
            store: Ext.create('TradeStorm.store.Symbols', {})
        }, {
            xtype: 'datefield',
            name: 'contract_exp',
            fieldLabel: 'Expr. Date',
            margin: '0 5 0 0',
            allowBlank: false
        }, {
            xtype: 'radiogroup',
            fieldLabel: 'Side',
            items: [{
                boxLabel: 'Buy',
                name: 'side',
                inputValue: 'buy',
                checked: true
            }, {
                boxLabel: 'Sell',
                name: 'side',
                inputValue: 'sell',
                padding: '0 0 0 10px'
            }]
        }, {
            xtype: 'numberfield',
            fieldLabel: 'Lots',
            name: 'lots',
            value: 1,
            minValue: 1
        }, {
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: 'Trader',
            name: 'trader',
            emptyText: 'trader'
        }, {
            xtype: 'radiogroup',
            fieldLabel: 'Type',
            items: [
                {
                    boxLabel: 'Limit',
                    name: 'offer_type',
                    inputValue: 'limit',
                    width: '27.3%',
                    listeners: {
                        change: 'limitSelected'
                    }
                },
                {
                    boxLabel: 'Market',
                    name: 'offer_type',
                    inputValue: 'market',
                    width: '27.3%',
                    checked: true,
                    listeners: {
                        change: 'limitSelected'
                    }
                },
                {
                    boxLabel: 'Pegged',
                    name: 'offer_type',
                    inputValue: 'pegged',
                    width: '27.3%',
                    listeners: {
                        change: 'peggedSelected'
                    }
                }
            ]
        },
        {
            xtype: 'numberfield',
            fieldLabel: 'Price',
            name: 'price',
            value: 0.00,
            minValue: 0.00,
            allowDecimals: true,
            decimalPrecision: 3,
            step: 0.050,
            disabled: false,
            reference: 'limit_price'
        }],
        buttons: [{
            text: 'Post Trade',
            formBind: true,
            handler: 'postTradeClick'
        }]
    }]
})