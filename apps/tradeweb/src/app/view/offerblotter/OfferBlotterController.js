Ext.define('TradeStorm.view.offerblotter.OfferBlotterController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.offerblotter',

    // This can be implemented later to get more trade 
    // information by clicking on a trade in the offerblotter.
    onItemSelected: function() {

    }
});