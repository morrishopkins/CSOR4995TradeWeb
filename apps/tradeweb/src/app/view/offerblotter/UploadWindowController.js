Ext.define('TradeStorm.view.offerblotter.UploadWindowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.uploadwindow',
    postTradeClick: function() {
        var viewPanel = this.view;
        var upForm = viewPanel.items.items[0].getForm();
        if (upForm.isValid()) {
            upForm.submit({
                success: function(upForm, action) {
                    console.log(upForm);
                    console.log(action);
                    Ext.Msg.alert('Success', action.result.msg);
                },
                failure: function(upForm, action) {
                    if(action.result.status == '200') {
                        viewPanel.close();
                        var offerstore = Ext.data.StoreManager.get("offers");
                        offerstore.reload();
                        var orderstore = Ext.data.StoreManager.get("orders");
                        setTimeout(function(){
                            orderstore.reload();
                        }, 2000);

                    } else {
                        Ext.Msg.alert('Failed', action.result.msg);
                    }

                }
            });
        }
    },
    limitSelected: function( rdio, newValue, oldValue, eOpts ){
        // field = this.lookupReference('limit_price');
        // field.enable();
    },

    peggedSelected: function( rdio, newValue, oldValue, eOpts ){
        // field = this.lookupReference('limit_price');
        // field.disable();
    }
});