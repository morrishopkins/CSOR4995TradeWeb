/**
 * List of trade position aggregated by trader.
 */
Ext.define('TradeStorm.view.offerblotter.PositionsTraderList', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.offers.PositionsTrader',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'positionstraderlist',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'positions_trader'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Trade Aggregate Report',
                fileName: 'positions_sym_trd.xml'
            });
        }
    }],
    columns: [
    {
        text: 'Trader',
        dataIndex: 'trader',
        flex: 1
    }, {
        text: 'Symbol',
        dataIndex: 'symbol',
        flex: 1
    }, {
        text: 'Lots',
        dataIndex: 'lots',
        flex: 5
    }]
});