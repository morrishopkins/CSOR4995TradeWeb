/**
 * This view is a list of all offers entered.
 */
Ext.define('TradeStorm.view.offerblotter.List', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.offers.Offers',
        'TradeStorm.view.offerblotter.UploadWindow',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'offerlist',
    layout: 'fit',
    ui: 'light-panel',
    border: false,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'offers'
    },


    title: 'Offer Blotter',
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Offer Report',
                fileName: 'myExport.xml'
            });
        }
    },{
        type: 'plus',
        tooltip: 'Add',
        callback: function(panel, tool, event) {
            Ext.create('uploadWindow').show();
        }
    }],


    columns: [{
        text: 'Symbol',
        dataIndex: 'symbol',
        flex: 1
    },
    {
        text: 'Offer Type',
        dataIndex: 'offer_type',
        flex: 1
    },
    {
        text: 'Offer Date',
        dataIndex: 'offer_date',
        flex: 1
    },
    {
        text: 'Transaction Time',
        dataIndex: 'transaction_time',
        flex: 1
    },

    {
        text: 'Expr. Date',
        dataIndex: 'contract_exp',
        flex: 1
    }, {
        text: 'Lots',
        dataIndex: 'lots',
        flex: 1
    }, {
        text: 'Price',
        dataIndex: 'price',
        flex: 1
    },
    {
        text: 'Side',
        dataIndex: 'side',
        flex: 1
    }, {
        text: 'Trader',
        dataIndex: 'trader',
        flex: 1
    }],
    listeners: {
        select: 'onItemSelected'
    }
});