/**
 * List of trade position aggregated by exp. date.
 */
Ext.define('TradeStorm.view.offerblotter.PositionsExpiryList', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.offers.PositionsExpiry',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'positionsexpirylist',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'positions_expiry'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Trade Aggregate Report',
                fileName: 'positions_sym_exp.xml'
            });
        }
    }],
    columns: [{
        text: 'Expr. Date',
        dataIndex: 'contract_exp',
        flex: 1
    }, {
        text: 'Symbol',
        dataIndex: 'symbol',
        flex: 1
    }, {
        text: 'Lots',
        dataIndex: 'lots',
        flex: 5
    }]
});