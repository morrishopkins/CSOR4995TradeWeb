/**
 * Panel holding all aggregate info. 
 */
Ext.define('TradeStorm.view.offerblotter.OfferAggregates', {
    extend: 'Ext.container.Container',
    xtype: 'offeraggregatescontainer',
    requires: [
        'Ext.ux.layout.ResponsiveColumn',
        'Ext.tab.Panel',
        'TradeStorm.view.offerblotter.PositionsList',
        'TradeStorm.view.offerblotter.PositionsExpiryList',
        'TradeStorm.view.offerblotter.PositionsTraderList',
        'TradeStorm.view.offerblotter.OfferBlotterController'
    ],
    controller: 'offerblotter',
    layout: 'fit',
    items: [
        {xtype: 'tabpanel',
         items: [
            {
                xtype: 'panel',
                title: 'Offers by Symbol',
                items: [{xtype: 'positionslist'}]
            },
            {
                xtype: 'panel',
                title: 'Offers by Symbol and Trader',
                items: [{xtype: 'positionstraderlist'}]
            },
            {
                xtype: 'panel',
                title: 'Offers by Symbol and Expr. Date',
                items: [{xtype: 'positionsexpirylist'}]
            }
         ]
        }

    ]
});
