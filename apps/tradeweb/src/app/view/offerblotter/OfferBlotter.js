/**
 * Panel holding trade blotter list of all trades.
 */
Ext.define('TradeStorm.view.offerblotter.OfferBlotter', {
    extend: 'Ext.container.Container',
    xtype: 'offerblottercontainer',
    requires: [
        'TradeStorm.view.offerblotter.List',
        'Ext.ux.layout.ResponsiveColumn',
        'TradeStorm.view.offerblotter.OfferBlotterController'
    ],
    controller: 'offerblotter',
    layout: 'fit',
    bodyPadding: 10,
    items: [
        {
            xtype: 'offerlist'
        }
    ]
});
