/**
 * List of trade position aggregated by symbol.
 */
Ext.define('TradeStorm.view.offerblotter.PositionsList', {
    extend: 'Ext.grid.Panel',
    requires: [
        'TradeStorm.store.offers.Positions',
        'Ext.grid.plugin.Exporter'
    ],

    xtype: 'positionslist',
    layout: 'fit',
    ui: 'light-panel',
    border: true,
    plugins: [{
        ptype: 'gridexporter'
    }],
    store: {
        type: 'positions'
    },
    tools: [{
        type: 'print',
        tooltip: 'Export',
        callback: function(panel, tool, event) {
            panel.saveDocumentAs({
                type: 'excel',
                title: 'Trade Aggregate Report',
                fileName: 'positions.xml'
            });
        }
    }],
    columns: [{
        text: 'Symbol',
        dataIndex: 'symbol',
        flex: 1
    }, {
        text: 'Lots',
        dataIndex: 'lots',
        flex: 6
    }]
});