Ext.define('TradeStorm.store.offers.Positions', {
    extend: 'Ext.data.Store',
    alias: 'store.positions',
    model: 'TradeStorm.model.offer.Positions',
    storeId: 'positions',
    proxy: {
         type: 'ajax',
         url: '/positions',
         reader: {
             type: 'json',
             rootProperty: ''
         }
     },
     autoLoad: true
});
