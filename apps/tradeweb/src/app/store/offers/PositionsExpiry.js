Ext.define('TradeStorm.store.offers.PositionsExpiry', {
    extend: 'Ext.data.Store',
    alias: 'store.positions_expiry',
    model: 'TradeStorm.model.offer.PositionsExpiry',
    storeId: 'positions_expiry',
    proxy: {
         type: 'ajax',
         url: '/positions_expiry',
         reader: {
             type: 'json',
             rootProperty: ''
         }
     },
     autoLoad: true
});
