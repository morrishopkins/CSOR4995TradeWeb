Ext.define('TradeStorm.store.offers.Offers', {
    extend: 'Ext.data.Store',
    alias: 'store.offers',
    model: 'TradeStorm.model.offer.Offer',
    storeId: 'offers',
    proxy: {
         type: 'ajax',
         url: '/offer',
         reader: {
             type: 'json',
             rootProperty: 'data'
         }
     },
     autoLoad: true
});
