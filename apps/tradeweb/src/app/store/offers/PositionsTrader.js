Ext.define('TradeStorm.store.offers.PositionsTrader', {
    extend: 'Ext.data.Store',
    alias: 'store.positions_trader',
    model: 'TradeStorm.model.offer.PositionsTrader',
    storeId: 'positions_trader',
    proxy: {
         type: 'ajax',
         url: '/positions_trader',
         reader: {
             type: 'json',
             rootProperty: ''
         }
     },
     autoLoad: true
});
