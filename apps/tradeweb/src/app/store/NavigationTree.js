Ext.define('TradeStorm.store.NavigationTree', {
    extend: 'Ext.data.TreeStore',

    storeId: 'NavigationTree',
    root: {
        expanded: true,
        children: [
            {
                text:   'Offer Blotter',
                view:   'offerblotter.OfferBlotter',
                leaf:   true,
                iconCls: 'fa fa-cubes',
                routeId: 'offerblotter'
            },
            {
                text:   'Order Blotter',
                view:   'orderblotter.OrderBlotter',
                leaf:   true,
                iconCls: 'fa fa-money',
                routeId: 'orderblotter'
            },
            {
                text:   'Swap Blotter',
                view:   'swapblotter.SwapBlotter',
                leaf:   true,
                iconCls: 'fa fa-exchange',
                routeId: 'swapblotter'
            }, {
                text: 'Reports',
                expanded: false,
                selectable: false,
                iconCls: 'fa fa-book',
                routeId : 'pages-parent',
                id:       'pages-parent',
                children: [
                    {
                        text: 'Offer Reports',
                        view: 'offerblotter.OfferAggregates',
                        leaf: true,
                        iconCls: 'fa fa-pdf-o',
                        routeId: 'aggregations'
                    },
                    {
                        text: 'Order Reports',
                        view: 'orderblotter.OrderAggregates',
                        leaf: true,
                        iconCls: 'fa fa-pdf-o',
                        routeId: 'orderreports'
                    },
                    {
                        text: 'Swap Reports',
                        view: 'swapblotter.SwapAggregates',
                        leaf: true,
                        iconCls: 'fa fa-pdf-o',
                        routeId: 'swapreports'
                    }
                ]
            }
        ]
    },
    fields: [
        {
            name: 'text'
        }
    ]
});
