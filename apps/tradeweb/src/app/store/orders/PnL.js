Ext.define('TradeStorm.store.orders.PnL', {
    extend: 'Ext.data.Store',
    alias: 'store.orderpnl',
    model: 'TradeStorm.model.order.PnL',
    storeId: 'orderpnl',
    proxy: {
         type: 'ajax',
         url: '/orderspnl',
         reader: {
             type: 'json',
             rootProperty: ''
         }
     },
     autoLoad: true
});
