Ext.define('TradeStorm.store.orders.Positions', {
    extend: 'Ext.data.Store',
    alias: 'store.orderpositions',
    model: 'TradeStorm.model.order.Positions',
    storeId: 'orderpositions',
    proxy: {
         type: 'ajax',
         url: '/orderpositions',
         reader: {
             type: 'json',
             rootProperty: ''
         }
     },
     autoLoad: true
});
