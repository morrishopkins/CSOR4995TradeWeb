Ext.define('TradeStorm.store.orders.Orders', {
    extend: 'Ext.data.Store',
    alias: 'store.orders',
    model: 'TradeStorm.model.order.Order',
    storeId: 'orders',
    proxy: {
         type: 'ajax',
         url: '/orders',
         reader: {
             type: 'json',
             rootProperty: 'data'
         }
     },
     autoLoad: true
});
