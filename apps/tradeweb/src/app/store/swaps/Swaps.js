Ext.define('TradeStorm.store.swaps.Swaps', {
    extend: 'Ext.data.Store',
    alias: 'store.swaps',
    model: 'TradeStorm.model.swap.Swap',
    storeId: 'swaps',
    proxy: {
         type: 'ajax',
         url: '/swaps',
         reader: {
             type: 'json',
             rootProperty: 'data'
         }
     },
     autoLoad: true
});
