Ext.define('TradeStorm.store.swaps.ClearingsDenied', {
    extend: 'Ext.data.Store',
    alias: 'store.swapsdenied',
    model: 'TradeStorm.model.swap.ClearingsDenied',
    storeId: 'swapsdenied',
    proxy: {
         type: 'ajax',
         url: '/swapsdenied',
         reader: {
             type: 'json',
             rootProperty: 'data'
         }
     },
     autoLoad: true
});
