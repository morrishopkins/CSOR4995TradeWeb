Ext.define('TradeStorm.store.swaps.PnL', {
    extend: 'Ext.data.Store',
    alias: 'store.swappnl',
    model: 'TradeStorm.model.swap.PnL',
    storeId: 'swappnl',
    proxy: {
         type: 'ajax',
         url: '/swapspnl',
         reader: {
             type: 'json',
             rootProperty: ''
         }
     },
     autoLoad: true
});
