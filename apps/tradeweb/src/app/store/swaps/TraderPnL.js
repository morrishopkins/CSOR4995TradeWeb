Ext.define('TradeStorm.store.swaps.TraderPnL', {
    extend: 'Ext.data.Store',
    alias: 'store.swaptraderpnl',
    model: 'TradeStorm.model.swap.TraderPnL',
    storeId: 'swaptraderpnl',
    proxy: {
         type: 'ajax',
         url: '/swapstraderpnl',
         reader: {
             type: 'json',
             rootProperty: ''
         }
     },
     autoLoad: true
});
