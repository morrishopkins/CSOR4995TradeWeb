 /**
  * The Model mapping to the Positions on the backend. Derived from Offer.
  */
 Ext.define('TradeStorm.model.offer.Positions', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'symbol'
        }
        ,{
            name: 'lots'
        }
    ]
 });
