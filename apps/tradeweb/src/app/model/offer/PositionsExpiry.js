 /**
  * The Model mapping to the Position Expiry on the backend. Derived from Offer.
  */
 Ext.define('TradeStorm.model.offer.PositionsExpiry', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'symbol'
        }
        ,{
            name: 'contract_exp'
        }
        ,{
            name: 'lots'
        }
    ]
 });
