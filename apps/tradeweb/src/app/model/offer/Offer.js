 /**
  * The Model mapping to the Offer on the backend.
  */
Ext.define('TradeStorm.model.offer.Offer', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'symbol'
        }
        ,{
            name: 'contract_exp',
            mapping: 'contract_exp.$date',
            convert: function(val){return new Date(val).toISOString().slice(0,10);}
        }
        ,{
            name: 'side'
        }
        ,{
            name: 'trader'
        }
        ,{
            name: 'price',
            convert: function(val){return (val == -999 || val == 0) ? '-' : val.toFixed(2);}
        }
        ,{
            name: 'lots'
        }
        ,{
            name: 'offer_type'
        }
        ,{
            name: 'offer_date',
            mapping: 'offer_date.$date',
            convert: function(val){return new Date(val).toISOString().slice(0,10);}
        }
        ,{
            name: 'transaction_time',
            mapping: 'transaction_time.$date',
            convert: function(val){return new Date(val).toTimeString().split(' ')[0];}
        }
    ]
 });
