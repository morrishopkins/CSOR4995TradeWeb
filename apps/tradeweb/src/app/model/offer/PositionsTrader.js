/**
* The Model mapping to the Positions Trader on the backend. Derived from Offer.
*/
Ext.define('TradeStorm.model.offer.PositionsTrader', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'symbol'
        }
        ,{
            name: 'trader'
        }
        ,{
            name: 'lots'
        }
    ]
 });
