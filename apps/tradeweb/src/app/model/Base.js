/**
 * This class has been implemented so that common
 * functionality can be added later without worrying
 * about modifying each subclass. Currently not being used.
 */
Ext.define('TradeStorm.model.Base', {
    extend: 'Ext.data.Model',

    schema: {
        namespace: 'TradeStorm.model'
    }
});
