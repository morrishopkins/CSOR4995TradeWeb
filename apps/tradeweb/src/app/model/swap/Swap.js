 /**
  * The Model mapping to the Swap on the backend.
  */
Ext.define('TradeStorm.model.swap.Swap', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'start_date',
            mapping: 'start_date.$date',
            convert: function(val){return new Date(val).toISOString().slice(0,10);}
        },
        {
            name: 'termination_date',
            mapping: 'termination_date.$date',
            convert: function(val){return new Date(val).toISOString().slice(0,10);}
        },
        {
            name: 'floating_rate'
        },
        {
            name: 'spread',
            convert: function(val){return (val == -999 || val == 0) ? '-' : val.toFixed(2);}
        },
        {
            name: 'fixed_rate',
            convert: function(val){return (val == -999 || val == 0) ? '-' : val.toFixed(2);}
        },
        {
            name: 'fixed_payee'
        },
        {
            name: 'float_payee'
        },
        {
            name: 'trader'
        },
        {
            name: 'transaction_time',
            mapping: 'transaction_time.$date',
            convert: function(val){return new Date(val).toTimeString().split(' ')[0];}
        }
    ]
 });

