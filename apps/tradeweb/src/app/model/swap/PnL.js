 /**
  * The Model mapping to the PnL on the backend. Derived from Order.
  */
 Ext.define('TradeStorm.model.swap.PnL', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'start_date',
            mapping: 'start_date.$date',
            convert: function(val){return new Date(val).toISOString().slice(0,10);}
        },
        {
            name: 'termination_date',
            mapping: 'termination_date.$date',
            convert: function(val){return new Date(val).toISOString().slice(0,10);}
        },
        {
            name: 'floating_rate',
            convert: function(val){return (val == -999 || val == 0) ? '-' : val.toFixed(2);}
        },
        {
            name: 'fixed_rate',
            convert: function(val){return (val == -999 || val == 0) ? '-' : val.toFixed(2);}
        },
        {
            name: 'spread',
            convert: function(val){return (val == -999 || val == 0) ? '-' : val.toFixed(2);}
        },
        {
            name: 'pay_receive',
            convert: function(val){return (val == 1) ? 'trader' : 'exchange';}
        },
        {
            name: 'trader'
        },
        {
            name: 'pnl',
            convert: function(val){return (val == -999) ? '-' : val.toFixed(2);}
        }
    ]
 });
