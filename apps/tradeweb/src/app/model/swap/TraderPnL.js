 /**
  * The Model mapping to the PnL on the backend. Derived from Order.
  */
 Ext.define('TradeStorm.model.swap.TraderPnL', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'trader'
        },
        {
            name: 'pnl',
            convert: function(val){return (val == -999) ? '-' : val.toFixed(2);}
        }
    ]
 });
