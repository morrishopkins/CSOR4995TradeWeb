/**
 * Basic symbol model. Created for the purpose of extensibility.
 */
Ext.define('TradeStorm.model.Symbol', {
    extend: 'TradeStorm.model.Base',
    fields: [
        'symbol'
    ]
});