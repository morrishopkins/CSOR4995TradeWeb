/**
 * This model corresponds to a real, live marked data for an arbitrary list of stock symbols.
 * This is outside the scope of Checkpoint 1 and was created to enhance our application.
 */
Ext.define('TradeStorm.model.MarketData', {
    extend: 'TradeStorm.model.Base',
    fields: [
        'symbol',
        'open',
        'high',
        'low',
        'close',
        'adjClose',
        'volume'
    ]
});

