 /**
  * The Model mapping to the PnL on the backend. Derived from Order.
  */
 Ext.define('TradeStorm.model.order.PnL', {
    extend: 'TradeStorm.model.Base',
    fields: [
        {
            name: 'symbol'
        },
        {
            name: 'contract_exp',
            mapping: 'contract_exp.$date',
            convert: function(val){if(val) {
                            return new Date(val).toISOString().slice(0,10);
                        } else {
                            return '';
                        }}
        },
        {
            name: 'pnl',
            convert: function(val){return (val == -999) ? '-' : val.toFixed(2);}
        }
    ]
 });
