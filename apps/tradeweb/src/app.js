/*
 * This file is generated and updated by Sencha Cmd. You can edit this file as
 * needed for your application, but these edits will have to be merged by
 * Sencha Cmd when upgrading.
 */
Ext.application({
    name: 'TradeStorm',

    extend: 'TradeStorm.Application',

    requires: [
        'TradeStorm.Application',
        'TradeStorm.model.Base',
        'TradeStorm.model.MarketData',
        'TradeStorm.model.offer.Offer',
        'TradeStorm.model.swap.Swap',
        'TradeStorm.store.NavigationTree',
        'TradeStorm.store.offers.Offers',
        'TradeStorm.store.swaps.Swaps',
        'TradeStorm.view.main.MainContainerWrap',
        'TradeStorm.view.main.Viewport',
        'TradeStorm.view.main.ViewportController',
        'TradeStorm.view.main.ViewportModel',
        'TradeStorm.view.marketdata.MarketData',
        'TradeStorm.view.marketdata.MarketDataController',
        'TradeStorm.view.marketdata.MarketDataModel',
        'TradeStorm.view.pages.BlankPage',
        'TradeStorm.view.pages.Error404Window',
        'TradeStorm.view.pages.Error500Window',
        'TradeStorm.view.swapblotter.SwapBlotter'
    ],

    mainView: 'TradeStorm.view.main.Viewport'

    //-------------------------------------------------------------------------
    // Most customizations should be made to TradeStorm.Application. If you need to
    // customize this file, doing so below this section reduces the likelihood
    // of merge conflicts when upgrading to new versions of Sencha Cmd.
    //-------------------------------------------------------------------------
});
