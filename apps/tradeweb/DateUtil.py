from sets import Set
from datetime import datetime, timedelta

OPEN_HOUR = 9
OPEN_MIN = 30
CLOSE_HOUR = 16
CLOSE_MIN = 0
EARLY_CLOSE_HOUR = 13

# initializing the BUSINESS DAY TO Sept. 03, 1783. This will be easily
# recognizable in the system to let us know if the system business day
# calculations are being processed correctly at startup.
BUSINESS_DAY = datetime.strptime('17830903', '%Y%m%d')

HOLIDAYS = Set([
  '20150101',
  '20150119',
  '20150216',
  '20150403',
  '20150525',
  '20150703',
  '20150907',
  '20151126',
  '20151225',
  '20160101',
  '20160118',
  '20160215',
  '20160325',
  '20160530',
  '20160704',
  '20160905',
  '20161124',
  '20161226'
  ])

EARLY_CLOSE = Set([
  '20151127',
  '20161125',
  '20161224',
  '20161224'
  ])


class TradeDateUtil(object):

  @staticmethod
  def get_business_day():
    '''Method used to get value for the business date. This is a dumb method
    which makes no assumptions about how the returned business day value will
    be used.'''
    business_day = datetime.now()
    #business_day_string = business_day.strftime('%Y%m%d')
    if(business_day.hour >= CLOSE_HOUR):
      print business_day
      business_day += timedelta(days=1)
      business_day= business_day.replace(hour=OPEN_HOUR, minute=OPEN_MIN)
      print business_day
      print "---------"
    if business_day.weekday() == 5:
      print business_day
      business_day += timedelta(days=2)
      business_day=business_day.replace(hour=OPEN_HOUR, minute=OPEN_MIN)
      print business_day
      print "---------------h"
    if business_day.weekday() == 6:
      print business_day
      business_day += timedelta(days=1)
      business_day=business_day.replace(hour=OPEN_HOUR, minute=OPEN_MIN)
      print business_day
      print "----------------4"
    business_day_string = business_day.strftime('%Y%m%d')

    while (business_day_string in HOLIDAYS or
             (business_day_string in EARLY_CLOSE and
                  business_day.hour >= EARLY_CLOSE_HOUR)):



        print HOLIDAYS
        business_day += timedelta(days=1)
        business_day=business_day.replace(hour=OPEN_HOUR, minute=OPEN_MIN)
        business_day_string=business_day.strftime('%Y%m%d')
        print business_day_string
        print business_day
    return business_day


  @staticmethod
  def set_business_day(businessdate):
    '''This function is used to update the business day for testing purposes.
    This should not be called by a production system.'''
    global BUSINESS_DAY
    BUSINESS_DAY = businessdate

  @staticmethod
  def inside_current_trading_hours(some_datetime):
    some_datetime_string = some_datetime.strftime('%Y%m%d')
    open_time = some_datetime.replace(hour=OPEN_HOUR, minute=OPEN_MIN, second=0)
    close_time = some_datetime.replace(hour=CLOSE_HOUR, minute=CLOSE_MIN, second=0)
    if some_datetime >= close_time or some_datetime <= open_time:
      return False
    elif some_datetime_string in HOLIDAYS:
      return False
    elif some_datetime_string in EARLY_CLOSE:
      early_close_time = some_datetime.replace(hour=EARLY_CLOSE_HOUR, minute=CLOSE_MIN, second=0)
      if some_datetime >= early_close_time:
        return False
    return True

  @staticmethod
  def get_endofmonth_business_day(some_datetime, prior_days):
    months = {'1': 31, '2': 28, '3':31, '4':30, '5':31, '6':30, '7':31, '8':31, '9':30, '10':31, '11':30, '12':31}
    d_month = some_datetime.month
    newdate = some_datetime.replace(day=months[str(some_datetime.month)])
    idx = 0
    while idx < prior_days:
      if newdate.weekday() == 5:
        newdate -= timedelta(days=1)
      if newdate.weekday() == 6:
        newdate -= timedelta(days=2)
      newdate_string = newdate.strftime('%Y%m%d')
      if newdate_string not in HOLIDAYS:
        newdate -= timedelta(days=1)
        idx += 1

    return newdate

