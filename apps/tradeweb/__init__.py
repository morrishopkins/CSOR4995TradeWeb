from flask import Flask
from flask.ext.mongoengine import MongoEngine
from flask.ext.script import Manager, Server
from ConfigParser import SafeConfigParser
from bs4 import BeautifulSoup
# from apps.tradeweb.models.InterestRateSwap import InterestRateSwap as Swap
import DateUtil
import pika
import threading

app = Flask(__name__, static_folder = 'src')
app.config["MONGODB_SETTINGS"] = {'DB': "tradeweb"}
app.config["SECRET_KEY"] = "3^st_0f-E@D3n"
db = MongoEngine(app)
tu = DateUtil
tw_queue_config = {}
queuedata = None
connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost',
        heartbeat_interval=2000))
channel = connection.channel()
manager = Manager(app)

config_file_path = None
offer_pub_qname = None
read_heart_qname = None
resp_heart_qname = None
failover_qname = None
pubswap_qname = None
readswap_qname = None
HEARTBEAT_STARTED = None



def get_pubswapqname():
    return pubswap_qname


def get_readswapqname():
    return readswap_qname


import apps.tradeweb.models
import apps.tradeweb.routes
from apps.tradeweb._models.InterestRateSwap import InterestRateSwap as Swap

if __name__ == '__main__':
  app.run()

def tw_heartbeat_read(channel, method_frame, header_frame, body):
    print "TW read heartbeat from hearbeat server."
    channel.basic_publish(exchange='', routing_key='tw_resp_heart', body='1')
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)


def consume_heartbeats(qname):
    global HEARTBEAT_STARTED
    if not HEARTBEAT_STARTED:
        try:
            channel.basic_consume(tw_heartbeat_read, qname)
        except Exception as e:
            print e
            print 'Error while consuming heartbeat.'
        # channel.start_consuming()
        HEARTBEAT_STARTED = True


def swap_read(channel, method_frame, header_frame, body):
    ast = BeautifulSoup(body, "xml")  # gives us ast of the xml string
    uuid = None
    if ast.clearingConfirmed:
        uuid = ast.clearingConfirmed.trade.tradeId["uuid"]
        Swap.set_confirmed(uuid)
        print 'Clearing Confirmed.'
    if ast.clearingDenied:
        uuid = ast.clearingDenied.trade.tradeId["uuid"]
        Swap.set_denied(uuid)
        print 'Clearing Denied.'
    print body


def consume_clearing_messages(qname):
    try:
        channel.basic_consume(swap_read, qname)
    except Exception as e:
        print e
        print 'Error while consuming swap clearing response.'


def runserver_conf(conf='tw_active.conf'):
    global config_file_path
    config_file_path = conf
    return Server(
        use_debugger = True,
        use_reloader = True,
        host = '0.0.0.0')

def consume_failover_messages(qname):
    '''This function catches a signal from rabbitmq indicating that
    the fixclient has failed.
    '''
    try:
        channel.basic_consume(update_queues_on_fcfailover, qname)
    except Exception as e:
        print e
        print 'Error while consuming failover message.'


def update_queues_on_fcfailover(channel, method_frame, header_frame, body):
    global offer_pub_qname
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)
    channel.basic_cancel(offer_pub_qname)
    offer_pub_qname = str(queuedata.get('publishoffer_bk', 'newoffer1'))
    channel.queue_declare(queue=offer_pub_qname)
    print 'Set queue to begin publishing messages to: ', offer_pub_qname


manager.add_command("runbackup", runserver_conf('tw_passive.conf1'))
manager.add_command("runserver", runserver_conf())


def setup_consumer():
    global config_file_path
    global offer_pub_qname
    global read_heart_qname
    global resp_heart_qname
    global failover_qname
    global pubswap_qname
    global readswap_qname
    print 'running primary'
    print config_file_path
    parser = SafeConfigParser()
    parser.read(config_file_path)
    queuedata = dict(parser.items('queues'))

    #This needs to be converted to use config file.
    offer_pub_qname = str(queuedata.get('publishoffer', 'newoffer'))
    read_heart_qname = str(queuedata.get('heartbeat_read', 'tw_read_heart'))
    resp_heart_qname = str(queuedata.get('heartbeat_resp', 'tw_resp_heart'))
    failover_qname = str(queuedata.get('failover', 'failover'))
    pubswap_qname = str(queuedata.get('publish_swap', 'tw_swap'))
    readswap_qname = str(queuedata.get('read_swap', 'tw_read_swap'))


    channel.queue_declare(queue=read_heart_qname)
    channel.queue_declare(queue=resp_heart_qname)
    channel.queue_declare(queue=failover_qname)
    channel.queue_declare(queue=offer_pub_qname)
    channel.queue_declare(queue=pubswap_qname)
    channel.queue_declare(queue=readswap_qname)

    try:
        consume_heartbeats(read_heart_qname)
        consume_clearing_messages(readswap_qname)
        consume_failover_messages(failover_qname)
        channel.start_consuming()
    except KeyboardInterrupt:
      channel.stop_consuming()


threading.Thread(target=setup_consumer).start()
