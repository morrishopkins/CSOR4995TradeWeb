# theme-tradestorm/sass/etc

This folder contains miscellaneous SASS files. Unlike `"theme-tradestorm/sass/etc"`, these files
need to be used explicitly.
