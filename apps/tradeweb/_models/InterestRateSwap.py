import datetime
from flask import url_for
from apps.tradeweb import db
import uuid
import swap_pnl_helper as pnl
dt=datetime


class InterestRateSwap(db.Document):
    '''this is the model of the IRS'''
    uuid=db.StringField(default=str(uuid.uuid1()),required=True)
    fixed_payee=db.StringField(required=True)
    float_payee=db.StringField(required=True)
    floating_rate=db.FloatField(required=True)
    fixed_rate=db.FloatField(required=True)
    has_expired = db.StringField(required=True,default="N")
    spread=db.FloatField(required=True)
    start_date=db.DateTimeField(default=datetime.datetime.now,required=True)
    termination_date=db.DateTimeField(default=datetime.datetime.now,required=True)
    trader=db.StringField(required=True)
    transaction_time = db.DateTimeField(default=datetime.datetime.now, required=True)
    cleared=db.BooleanField(default=None)

    def __unicode__(self):
        return self.uuid

    meta = {
        'allow_inheritance':True,
        'indexes' : ['trader','uuid'],
        'ordering' : ['trader','uuid']
    }


    @staticmethod
    def set_confirmed(uuid):
        InterestRateSwap.objects(uuid=uuid).update(cleared=True)


    @staticmethod
    def set_denied(uuid):
        InterestRateSwap.objects(uuid=uuid).update(cleared=False)


    @staticmethod
    def to_csv(expired_orders):
        # with open("exp_irs.csv",'w+') as fp:
        header="uuid,fixed_payee,float_payee,floating_rate," \
               "has_expired ,spread,start_date,termination_date," \
               "trader,transaction_time\n"

        for i in expired_orders:
            row=str(i.uuid)+','+ \
                str(i.fixed_payee)+','+ \
                str(i.float_payee)+','+ \
                str(i.floating_rate)+','+ \
                str(i.has_expired )+','+ \
                str(i.spread)+','+ \
                str(i.start_date)+','+ \
                str(i.termination_date)+','+ \
                str(i.trader)+','+ \
                str(i.transaction_time )+'\n'
            header += row
        return header


    @staticmethod
    def swap_pnl():
        '''
        Start date, expiration date, fixed,
        float, spread, pay/receive (1/0), PnL
        returns a list of not expired swaps to be
        converted to json
        :return:
        '''
        lpnl=[]
        row={}
        for i in InterestRateSwap.objects(cleared=True):
            if i.has_expired=="Y":
                continue
            row["trader"]=i.trader
            row["start_date"]=str(i.start_date)
            row["termination_date"]=str(i.termination_date)
            row["fixed_rate"]=i.fixed_rate
            row["floating_rate"]=i.floating_rate
            row["spread"]=i.spread
            row["pay_receive"]= 1 if i.fixed_payee=="trader" else 0
            row["pnl"]=pnl.swap_pnl(dt.datetime.now(),i.termination_date, i.fixed_rate,i.spread,row["pay_receive"])
            lpnl.append(row)

        return lpnl


    @staticmethod
    def swap_pnl_by_trader():
        trader_swap_pnl={}
        spl =InterestRateSwap.swap_pnl()
        for i in spl:
            try:
                if not trader_swap_pnl.has_key(i["trader"]):
                    trader_swap_pnl[i["trader"]]=0
                trader_swap_pnl[i["trader"]]+=i["pnl"]
            except:
                pass
        traderpnl=[]
        for k,v in trader_swap_pnl.iteritems():
            x = {}
            x['trader'] = k
            x['pnl'] = v
            traderpnl.append(x)
        return traderpnl



