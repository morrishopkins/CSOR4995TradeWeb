import datetime as dt
import random
import collections


def payment_dates(currentdate,expirationdate):
    '''
    this function returns a list of dates for which the swap payments need to be exchanged
    Takes as input the currentdate (the date at which the report is to be generated) and
    the expiration date of the IRS
    '''
    l=[]
    if ((currentdate-dt.datetime(currentdate.year,7,1)).days<0):
        l.append(dt.datetime(currentdate.year,7,1))
    else:
        l.append(dt.datetime(currentdate.year+1,1,1))
    temp=l[0]
    while temp<expirationdate:
        if temp.month==1:
            temp=dt.datetime(temp.year,7,1)
            l.append(temp)
        else:
            temp=dt.datetime(temp.year+1,1,1)
            l.append(temp)
    if l[-1]!=expirationdate:
        l.append(expirationdate)
    del l[-2]
    return l




def term_struct(currentdate,dates,base_term_struct):
    '''
    takes as input the current date,the list of dates on which payment has to be
    exchanged and the term structure of interest rates. Returns a term structure for relevant
    dates as a map.
    '''
    di={}
    #print base_term_struct
    for elem in dates:
        length= (elem-currentdate).days/float(365)
        for i in range(1,len(base_term_struct)):
            k1,v1=base_term_struct.items()[i]
            #print k1
            if k1>=length:
                k2,v2=base_term_struct.items()[i-1]
                di[elem]=round(v2 + length*((v2-v1)/float((k2-k1))),2)
                break
    return di


def update_term_struct(base_term_struct):
    '''
    this function updates the term structure of interest rate everyday.
    '''
    for key in base_term_struct:
        base_term_struct[key]=round(base_term_struct[key] + key*random.uniform(-1,1)*.001,3)
    return base_term_struct


def irs_pnl(rates,currentdate,f):
    '''
    Takes the ordered dictionary of payment dates and the term structure and
    the corresponding rates, the difference between the fixed rate and the spread
    and returns the pnl
    '''
    npv=0
    i=1
    length=len(rates)
    for key in rates:
        datediff=(key-currentdate).days
        compound_factor=datediff/float(180)
        temp=rates[key]
        if i==length:
            break
        npv=npv+ (10000*f)/pow((float(1+ rates[key]/float(200))),compound_factor)
        i=i+1
    npv = npv + (10000*f) + 1000000/pow((float(1+ temp/float(200))),compound_factor)
    return (npv -1000000)


def swap_pnl(currentdate,expirationdate,fixed_rate,spread,pay):
    '''
    currentdate -> date at which PnL needs to be calculated in dt.datetime(yyyy,mm,dd) format
    expirationdate -> the date on which the contract expires same format
    fixed_rate -> fixed rate of the swap as a float. so 2.0 and not 2%
    spread -> spread over LIBOR as a float
    pay -> this will be 1 or 0. 1 if the trader pays fixed and 0 if the trader pays floating
    '''
    base_term_struct={}
    base_term_struct[0]=0
    base_term_struct[.25]=.07
    base_term_struct[.5]=.09
    base_term_struct[1]=.13
    base_term_struct[2]=.39
    base_term_struct[3]=.76
    base_term_struct[5]=1.72
    base_term_struct[7]=2.41
    base_term_struct[10]=3
    base_term_struct[20]=3.68
    base_term_struct[30]=4.04
    base_term_struct[50]=5.04
    base_term_struct=collections.OrderedDict(sorted(base_term_struct.items()))
    list_date= payment_dates(currentdate,expirationdate)
    base_term_struct=update_term_struct(base_term_struct)
    newt= term_struct(currentdate,list_date,base_term_struct)
    newt= collections.OrderedDict(sorted(newt.items()))
    pnl=irs_pnl(newt,currentdate,fixed_rate-spread)
    if pay==1:
        return pnl
    else:
        return -1*pnl



#testing purpose
swap_pnl(dt.datetime(2015,12,1),dt.datetime(2042,4,1),3.1,1,0)
