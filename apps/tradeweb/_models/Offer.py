import datetime
from flask import url_for
from apps.tradeweb import db

class Offer(db.Document):
    '''this is the model for our Offer item which is rendered in the web gui
    we are extending a mongo document,  this gives us the ability to interact with
    mongo db items as if they were python objects'''
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    symbol = db.StringField(max_length=32, required=True)
    contract_exp = db.DateTimeField(required=True)
    side = db.StringField(max_length=8, required=True) #Buy or Sell
    lots = db.IntField(required=True)
    trader = db.StringField(required=True)
    price = db.FloatField(required=True)
    offer_type = db.StringField(required=True)
    offer_date = db.DateTimeField(default=datetime.datetime.now, required=True)
    transaction_time = db.DateTimeField(default=datetime.datetime.now, required=True)
    has_expired = db.StringField(required=True, default="N")

    def __unicode__(self):
        return self.symbol

    meta = {
        'allow_inheritance': True,
        'indexes': ['trader', 'symbol'],
        'ordering': ['-offer_date']
    }

    @staticmethod
    def calculate_position_by_sym():
      '''this returns a dictionary which has the positions calculated by symbol'''
      position_data = []
      for offer in Offer.objects:
        try:
          position_by_sym={}
          position_by_sym['symbol'] = offer.symbol
          if not position_by_sym.has_key(offer.symbol):
            position_by_sym['lots']=0
          if offer.side.lower()=='buy':
            position_by_sym['lots']+=offer.lots
          else:
            position_by_sym['lots']-=offer.lots
          position_data.append(position_by_sym)
        except:
          pass
      return position_data

    @staticmethod
    def calculate_positions_by_trader():
      '''this returns a dictionary with positions for each symbol grouped by trader'''
      trader_position={}

      for offer in Offer.objects:
        try:
          if not trader_position.has_key(offer.trader):
            trader_position[offer.trader]={}

          if not trader_position[offer.trader].has_key(offer.symbol):
            trader_position[offer.trader][offer.symbol]=0

          if offer.side.lower()=='buy':
            trader_position[offer.trader][offer.symbol]+=offer.lots
          else:
            trader_position[offer.trader][offer.symbol]-=offer.lots
        except:
          pass
      position_data = []
      for t, pos in trader_position.iteritems():
        for k,v in pos.iteritems():
          pdata = {}
          pdata['trader'] = t
          pdata['symbol'] = k
          pdata['lots'] = v
          position_data.append(pdata)
      return position_data

    @staticmethod
    def calculate_positions_by_expiry():
      '''this returns a dictionary with positions for each symbol grouped by expiry'''
      expiry_position={}

      for offer in Offer.objects:
        try:
          tstamp = offer.contract_exp.strftime("%Y-%m-%d")
          if not expiry_position.has_key(tstamp):
            expiry_position[tstamp]={}

          if not expiry_position[tstamp].has_key(offer.symbol):
           expiry_position[tstamp][offer.symbol]=0

          if offer.side.lower()=='buy':
            expiry_position[tstamp][offer.symbol]+=offer.lots
          else:
            expiry_position[tstamp][offer.symbol]-=offer.lots
        except:
          pass
      position_data = []
      for t, pos in expiry_position.iteritems():
        for k,v in pos.iteritems():
          pdata = {}
          pdata['contract_exp'] = t
          pdata['symbol'] = k
          pdata['lots'] = v
          position_data.append(pdata)
      return position_data






