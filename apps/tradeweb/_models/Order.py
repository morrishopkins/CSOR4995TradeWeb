import datetime
import numpy
from flask import url_for
from apps.tradeweb import db
import mongoengine

class Order(db.Document):
    ''' This is the model for our Order item which is rendered in the web gui
    we are extending a mongo document,  this gives us the ability to interact with
    mongo db items as if they were python objects '''
    contract_exp = db.DateTimeField(required=True)
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    lots = db.IntField(required=True)
    order_id = db.IntField(required=True)
    order_type = db.StringField(max_length=8, required=True)
    price = db.FloatField(required=True)
    side = db.StringField(max_length=8, required=True) #Buy or Sell
    symbol = db.StringField(max_length=32, required=True)
    trade_date = db.DateTimeField(default=datetime.datetime.now, required=True)
    trader = db.StringField(required=True)
    transaction_time = db.DateTimeField(default=datetime.datetime.now, required=True)
    has_expired = db.StringField(required=True, default="N")
    #this will be y or n

    def __unicode__(self):
        return self.symbol

    meta = {
        'allow_inheritance': True,
        'indexes': ['trader', 'symbol'],
        'ordering': ['-trade_date']
    }

    @staticmethod
    def calculate_positions_by_expiry():
      '''this returns a dictionary with positions for each symbol grouped by expiry'''
      expiry_position={}
      total_prices={}

      for order in Order.objects:
        try:
          tstamp = order.contract_exp.strftime("%Y-%m-%d")

          if not expiry_position.has_key(tstamp):
            expiry_position[tstamp]={}
          if not total_prices.has_key(tstamp):
            total_prices[tstamp]={}

          if not expiry_position[tstamp].has_key(order.symbol):
           expiry_position[tstamp][order.symbol]=0
          if not total_prices[tstamp].has_key(order.symbol):
           total_prices[tstamp][order.symbol]=0

          if order.side.lower()=='buy':
            expiry_position[tstamp][order.symbol]+=order.lots
            total_prices[tstamp][order.symbol]+=(order.lots * order.price)
          else:
            expiry_position[tstamp][order.symbol]-=order.lots
            total_prices[tstamp][order.symbol]+=(order.lots * order.price * -1)
        except:
          pass
      position_data = []
      for t, pos in expiry_position.iteritems():
        for k,v in pos.iteritems():
          pdata = {}
          pdata['contract_exp'] = t
          pdata['symbol'] = k
          pdata['lots'] = v
          pdata['price'] = (total_prices[t][k] / v)
          position_data.append(pdata)
      return position_data

    @staticmethod
    def market_data_pnl():
        ticker_list = ['LN','NN','NG','NP','HH','ON','HP','PD','HB','TC','PH','NR','TR','5Z','NL','GL','PG','IN','NH','9F','PW','NW','CI','G4','6J','NX','NK','CZ','NJ','ZR','E7','TME','SZ','IX','PM','DSF','NE','PJ','FP','NF','TDE','TZ6','NZ','XJ','Q1','PF','TX','8X','PE','G3','CFS','NM','QG','6Z','DVS','ZN','8Z','G2','MFS','CW','PK','G6','ND','XR','G7','M8','B4','WFS','PFS','NME','IV','XN','Z5P','U07','B2','ZTS','NS','DVA','DAC','U12','IS','LAI','XZ4','XNC','XZ','5F','T4','SK','GME','N5','U30','TZ','N4','SS','Q2','DUE','V8','U23','PU','VX','U05','IJ','LN3','X5','L2','DI','9A','ON1','5E','U14','IU','HHT','TZI','ZNP','KD','OX','XSC','Y7','5M','T7','SM','DNG','HBI','G5','SU','Q9','IPE','K7','U25','XH','IL','LN5','VNQ','Z8','GND','DW','C4','ON3','5H','U16','IW','NNT','TZ5','DML','ZNT','IC','PC','JKM','XAC','SX','SF','GRE','M3','U27','U09','IP','DPP','VNA','ZTA','IM','SN','J6','U8','U02','IF','C9','ON5','W5','U18','IY','NB','DAL','MNG','XZ3','NV','XGC','XX','4D','T2','SJ','GNE','C7','M7','U29','XQ','PY','U11','IR','NFS','DCE','DNP','SR','UKE','T9','U22','VS','U04','II','IA','LN2','X4','U20','K8','A1','5D','U13','IT','TE','XZ6','OI','XKC','Y6','5I','T6','SL','UKD','N7','U31','HHS','ST','Q4','NVT','K6','U24','W9','U06','IK','LN4','Y9','GRD','DT','C3','ON2','5G','U15','8A','NGT','DFE','ZZ6','IB','PB','Y8','5O','T8','XIC','SV','S9','DPE','L4','U26','4W','U08','CFP','VNS','NDE','M6','U01','E2','C8','ON4','5N','U17','8E','TZS','DAR','IE','NQ','XZ1','XFC','5K','SY','SH','GGE','M4','U28','XO','6I','U10','IQ','TEP','SGW','IZ','SQ','PX','ASE','J8','U21','VI','U03','IH','LN1','X2','U19','J3']
        d={}
        for ticker in ticker_list:
            rv= 40* numpy.random.lognormal(.05,0.15)
            d[ticker] = round(rv,2)
        return d

    @staticmethod
    def calculate_pnl():
      '''this returns a dictionary with positions for each symbol exp and pnl'''
      pnl_position={}
      pricelist = Order.market_data_pnl()
      for order in Order.objects:
        try:
          tstamp = order.contract_exp.strftime("%Y-%m-%d")

          if not pnl_position.has_key(tstamp):
            pnl_position[tstamp]={}

          if not pnl_position[tstamp].has_key(order.symbol):
           pnl_position[tstamp][order.symbol]=0

          if order.side.lower()=='buy':
            pnl_position[tstamp][order.symbol]+=round((order.lots*(pricelist[order.symbol] - order.price)),2)
          else:
            pnl_position[tstamp][order.symbol]-=round((order.lots*(pricelist[order.symbol] - order.price)),2)
        except:
          pass
      position_data = []
      for t, pos in pnl_position.iteritems():
        for k,v in pos.iteritems():
          pdata = {}
          pdata['contract_exp'] = t
          pdata['symbol'] = k
          pdata['pnl'] = v
          position_data.append(pdata)
      return position_data


    @staticmethod
    def calculate_traderpnl():
      '''this returns a dictionary with positions for each pnl grouped by trader'''
      pnl_position={}
      pricelist = Order.market_data_pnl()
      for order in Order.objects:
        try:
          trader = order.trader

          if not pnl_position.has_key(trader):
            pnl_position[trader]=0

          if order.side.lower()=='buy':
            pnl_position[trader]+=round((order.lots*(pricelist[order.symbol] - order.price)),2)
          else:
            pnl_position[trader]-=round((order.lots*(pricelist[order.symbol] - order.price)),2)
        except:
          pass
      position_data = []
      for t, p in pnl_position.iteritems():
          pdata = {}
          pdata['trader'] = t
          pdata['pnl'] = p
          position_data.append(pdata)
      return position_data

    @staticmethod
    def to_csv(expired_orders):
        '''
        :param expired_orders: this is the list of orders expiring today
        :return: csv
        '''
        header="contract_exp,created_at,lots,order_id,order_type,"+\
            "price,side,symbol,trade_date,trader,transaction_time,has_expired\n"
        for i in expired_orders:
            row = i.contract_exp.strftime("%Y-%m-%d")+","+\
            i.created_at.strftime("%Y-%m-%d")+","+\
            str(i.lots)+","+\
            str(i.order_id)+","+\
            i.order_type+","+\
            str(i.price)+","+\
            i.side+","+\
            i.symbol+","+\
            i.trade_date.strftime("%Y-%m-%d")+","+\
            i.trader+","+\
            str(i.transaction_time)+","+\
            str(i.has_expired)+'\n'
            header+=row
        return header



