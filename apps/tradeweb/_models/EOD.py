import datetime as dt
import pandas as pd
import numpy as np
import scipy as sp
from Order import *
from apps.tradeweb import db, tu
from apps.tradeweb._models.Order import Order
from apps.tradeweb._models.Offer import Offer
from apps.tradeweb._models.InterestRateSwap import InterestRateSwap
tu=tu.TradeDateUtil()

def get_expired_orders(kind):
    list_exp=[order for order in kind.objects if order.has_expired=='N']
    return list_exp



def EOD_Process(kind):
    ''' Needs to:
    - flag expired orders and swaps
    - newly flagged orders need to be output as csv
    - generate pnl reports for orders and swaps
    - rolls business day.
    '''
    biz_day=tu.get_business_day()
    print biz_day.date()
    expired_orders=[]

    for order in kind.objects:

        try:
            date=order.contract_exp.strftime("%Y-%m-%d")
            print date
            #print biz_day
            #print order.has_expired

            # if biz_day.date() == date.date():
            order.has_expired = 'Y'
            print order.has_expired
            order.save()
            expired_orders.append(order)
        except:
            continue
    dafile = kind.to_csv(expired_orders)
    print dafile
    return  dafile


def EOD_Order():
    return EOD_Process(Order)

def EOD_Swap():
    return EOD_Process(InterestRateSwap)
