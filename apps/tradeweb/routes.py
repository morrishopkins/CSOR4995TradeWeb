from flask import Flask, make_response, redirect, request, send_from_directory

from apps.tradeweb import app

@app.route('/')
def root():
  return app.send_static_file('index.html')

import apps.tradeweb._routes.marketdata
import apps.tradeweb._routes.offers
import apps.tradeweb._routes.orders
import apps.tradeweb._routes.swaps

@app.route('/<path:path>')
def static_proxy(path):
  # send_static_file will guess the correct MIME type
  return app.send_static_file(path)

@app.errorhandler(404)
def page_not_found(e):
    # your processing here
    return redirect('#404')

@app.errorhandler(500)
def page_not_found(e):
    # your processing here
    return redirect('#500')
