from flask import Flask
from flask.ext.mongoengine import MongoEngine
import datetime

app = Flask(__name__)
app.config["MONGODB_SETTINGS"] = {'DB': "tradeweb"}
app.config["SECRET_KEY"] = "3^st_0f-E@D3n"
db = MongoEngine(app)

class Order(db.Document):
    ''' This is the model for our Order item which is rendered in the web gui
    we are extending a mongo document,  this gives us the ability to interact with
    mongo db items as if they were python objects '''
    contract_exp = db.DateTimeField(required=True)
    created_at = db.DateTimeField(default=datetime.datetime.now, required=True)
    lots = db.IntField(required=True)
    order_id = db.IntField(required=True)
    order_type = db.StringField(max_length=8, required=True)
    price = db.FloatField(required=True)
    side = db.StringField(max_length=8, required=True) #Buy or Sell
    symbol = db.StringField(max_length=32, required=True)
    trade_date = db.DateTimeField(default=datetime.datetime.now, required=True)
    trader = db.StringField(required=True)
    transaction_time = db.DateTimeField(default=datetime.datetime.now, required=True)


    def __unicode__(self):
        return self.symbol

    meta = {
        'allow_inheritance': True,
        'indexes': ['trader', 'symbol'],
        'ordering': ['-trade_date']
    }
