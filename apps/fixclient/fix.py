import sys
import os
import time
import getopt
from datetime import datetime
import quickfix as fix
import pika
import json
from Order import Order
from ConfigParser import SafeConfigParser

CONF_FILE_PATH = '../../etc/fixclient.cfg'
connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

class FixApplication(fix.Application):
  orderID = 0
  execID = 0

  def gen_ord_id(self):
    global orderID
    orderID+=1
    return orderID

  def onCreate(self, sessionID):
    return

  def onLogon(self, sessionID):
    self.sessionID = sessionID
    print ("Successful Logon to session '%s'." % sessionID.toString())
    return

  def onLogout(self, sessionID):
    print "In client onLogout"
    return

  def toAdmin(self, sessionID, message):
    print "In client toAdmin"
    print message
    return

  def fromAdmin(self, sessionID, message):
    print "In client fromAdmin"
    print message
    return

  def toApp(self, sessionID, message):
    print "toApp Recieved the following message: %s" % message.toString()
    return

  def fromApp(self, message, sessionID):
    print "fromApp Recieved the folling message: %s" %message.toString()
    beginString = fix.BeginString()
    msgType = fix.MsgType()
    message.getHeader().getField(beginString)
    message.getHeader().getField(msgType)

    symbol = fix.Symbol()
    side = fix.Side()
    ordType = fix.OrdType()
    orderQty = fix.OrderQty()
    price = fix.Price()
    trader = fix.Text()
    exp_date = fix.ExpireDate()
    orderid = fix.OrderID()
    trade_date = fix.TradeDate()
    transaction_time = fix.EncodedText()

    message.getField(ordType)
    message.getField(symbol)
    message.getField(side)
    message.getField(orderQty)
    message.getField(price)
    message.getField(trader)
    message.getField(exp_date)
    message.getField(orderid)
    message.getField(trade_date)
    message.getField(transaction_time)


    # currently in place to simply test message connectivity.
    contract_exp = exp_date.getValue()
    order_id = orderid.getValue()
    order_type = ordType.getValue()
    lots = orderQty.getValue()
    px = price.getValue()
    sd = side.getValue()
    sym = symbol.getValue()
    tradedate = trade_date.getValue()
    trader_name = trader.getValue()
    transactiontime = transaction_time.getValue()

    print contract_exp
    print order_id
    print order_type
    print px
    print lots
    print sd
    print sym
    print tradedate
    print trader_name
    print transactiontime

    neworderstring = '{"contract_exp":"' + contract_exp + '",' +\
                     '"order_id":"' + order_id + '",' +\
                     '"order_type":"' + order_type + '",' +\
                     '"price":"' + str(px) + '",' +\
                     '"side":"' + sd + '",' +\
                     '"symbol":"' + sym + '",' +\
                     '"trade_date":"' + tradedate + '",' +\
                     '"trader":"' + trader_name + '",' +\
                     '"transaction_time":"' + transactiontime + '"}'

    print neworderstring
    otype = ''
    if order_type == fix.OrdType_MARKET:
      otype = 'market'
    elif order_type == fix.OrdType_LIMIT:
      otype = 'limit'
    elif order_type == fix.OrdType_PEGGED:
      otype = 'pegged'

    oside = ('sell' if fix.Side_SELL == sd else 'buy')

    print 'Adding new order to db.'
    try:
      neworder = Order(\
        contract_exp=datetime.strptime(contract_exp, "%Y%m%d"), \
        order_id=int(order_id), \
        order_type=otype, \
        price=px, \
        lots=lots, \
        side=oside, \
        symbol=sym, \
        trade_date=datetime.strptime(tradedate, "%Y%m%d"), \
        trader=trader_name, \
        transaction_time=datetime.strptime(transactiontime, "%H:%M:%S") )
      print 'about to save order'
      neworder.save()
      # neworderstring = neworder.to_json()
    except:
      print 'Aborting due to error inserting order into database'
    return

  def genOrderID(self):
  	self.orderID = self.orderID+1
  	return `self.orderID`

  def genExecID(self):
   	self.execID = self.execID+1
   	return `self.execID`

  def put_order(self, osymbol, side, qty, price, offer_type, contract_exp):
    print("Creating the following order: ")
    offer = fix.Message()
    offer.getHeader().setField(fix.BeginString(fix.BeginString_FIX42)) #
    offer.getHeader().setField(fix.MsgType(fix.MsgType_NewOrderSingle)) #39=D
    offer.setField(fix.ClOrdID(self.genExecID())) #11=Unique order
    offer.setField(fix.HandlInst(fix.HandlInst_MANUAL_ORDER_BEST_EXECUTION)) #21=3 (Manual order, best executiona)
    offer.setField(fix.Symbol(osymbol)) #55=SMBL ?
    offer.setField(fix.ExpireDate(contract_exp))
    offer.setField(fix.Side(side)) #43=1 Buy
    offer.setField(fix.OrdType(offer_type)) #40=2 Limit order
    offer.setField(fix.OrderQty(qty)) #38=100
    if (offer_type == fix.OrdType_LIMIT or offer_type == fix.OrdType_MARKET):
      offer.setField(fix.Price(price))
    elif offer_type is fix.OrdType_PEGGED:
      offer.setField(fix.ExecInst(fix.ExecInst_MID_PRICE_PEG))
    # offer.setField(fix.TransactTime(int(datetime.utcnow().strftime("%s"))))
    print offer.toString()
    fix.Session.sendToTarget(offer, self.sessionID)

#################################################################
# Requires that the fix server be started
#################################################################

def on_message(channel, method_frame, header_frame, body):
  print method_frame.delivery_tag
  print body
  parsed_json = json.loads(body)
  osymbol = parsed_json['symbol'].encode("utf-8")
  qty = parsed_json['lots']
  contract_exp = int(str(parsed_json['contract_exp'].get("$date"))[:10])
  exp_time = datetime.fromtimestamp(contract_exp).strftime('%Y%m%d')

  price = parsed_json['price']
  otype = parsed_json['offer_type'].encode("utf-8")
  offer_type = ''
  print "OTYPE:  '%s'." % otype
  if otype == 'market':
    print "Setting order type to market"
    offer_type = fix.OrdType_MARKET
  elif otype == 'limit':
    print "Setting order type to limit"
    offer_type = fix.OrdType_LIMIT
  elif otype == 'pegged':
    print "Setting order type to pegged"
    offer_type = fix.OrdType_PEGGED

  side = (fix.Side_SELL if parsed_json['side'].encode("utf-8") == 'sell' else fix.Side_BUY)
  # print osymbol
  # print side
  # print qty
  # print price
  # print offer_type
  application.put_order(osymbol, side, qty, price, offer_type, exp_time)
  print
  channel.basic_ack(delivery_tag=method_frame.delivery_tag)

def read_heartbeat(channel, method_frame, header_frame, body):
  print 'In read heartbeat'
  channel.basic_publish(exchange='', routing_key=resp_heart_qname, body="1")
  channel.basic_ack(delivery_tag=method_frame.delivery_tag)


def respond_to_tw_fail(channel, method_frame, header_frame, body):
  global ofr_consume_qname
  print 'stopping comsuming of messages from: ', ofr_consume_qname
  channel.basic_cancel(ofr_consume_qname)
  ofr_consume_qname = str(queuedata.get('consumeoffer_bk', 'newoffer2'))
  channel.queue_declare(queue=ofr_consume_qname)
  channel.basic_consume(on_message, ofr_consume_qname)
  print 'started consuming messages from: ', ofr_consume_qname
  channel.basic_ack(delivery_tag=method_frame.delivery_tag)



net_conf_file = ''
try:
  opts, args = getopt.getopt(sys.argv[1:],"hc:",["netconf="])
except getopt.GetoptError:
  print 'test.py -c <net_conf_file>'
  sys.exit(2)
for opt, arg in opts:
  if opt == '-h':
    print 'test.py -c <net_conf_file>'
    sys.exit()
  elif opt in ("-c", "--conf"):
    net_conf_file = arg
if len(net_conf_file) < 1:
  print 'Config file must be specified.'
  print 'test.py -c <net_conf_file>'
  sys.exit()

parser = SafeConfigParser()
parser.read(net_conf_file)
queuedata = dict(parser.items('queue'))
ofr_consume_qname = str(queuedata.get('consumeoffer', 'newoffer'))
read_heart_qname = str(queuedata.get('heartbeat_read', 'read_heart'))
resp_heart_qname = str(queuedata.get('heartbeat_resp', 'resp_heart'))
tw_failover_qname = str(queuedata.get('failover', 'failover'))

print "OFFR_PUB_Q: ", ofr_consume_qname
print "READ_HEART_Q: ", read_heart_qname
print "RESP_HEART_Q: ", resp_heart_qname

channel.queue_declare(queue=ofr_consume_qname)
channel.queue_declare(queue=read_heart_qname)
channel.queue_declare(queue=resp_heart_qname)
channel.queue_declare(queue=tw_failover_qname)
channel.basic_consume(on_message, ofr_consume_qname)
channel.basic_consume(read_heartbeat, read_heart_qname)
channel.basic_consume(respond_to_tw_fail, tw_failover_qname)
try:
  settings = fix.SessionSettings( CONF_FILE_PATH )
  application = FixApplication()
  storeFactory = fix.FileStoreFactory( settings )
  logFactory = fix.FileLogFactory( settings )
  initiator = fix.SocketInitiator( application, storeFactory, settings, logFactory )
  initiator.start()
  channel.start_consuming()
except KeyboardInterrupt:
  channel.stop_consuming()
except (fix.ConfigError, fix.RuntimeError), e:
  print e
connection.close()





