#fixclient
This is a standalone fix client application.
It is designed to consume messages from a rabbitmq broker, convert the messages into new order messages in FIX format, and submit the FIX messages to an exchange. When fill order messages are received from the exchange, these are converted into a more human friendly message format which is then published back to a rabbitmq broker.