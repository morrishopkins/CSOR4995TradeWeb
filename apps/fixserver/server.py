import sys
import time
import argparse
import quickfix as fix
import random
import numpy as np
import pandas as pd
import datetime as dt
import time as timee

CONF_FILE_PATH = '../../etc/server.cfg'
price_cache = {}
trader_names=['Sparks', 'Sullivan', 'Martinez', 'Hayes', 'Ball', 'Morales', 'Rhodes', 'Griffin', 'Rose', 'Ford', 'Mathis', 'Paul', 'Jones', 'Warner', 'Wallace', 'Sharp', 'Kim', 'Maxwell', 'Quinn', 'Barrett', 'Ballard', 'Obrien', 'Mclaughlin', 'Perry', 'Fisher']

class FixNode(object):
    def __init__(self, pr, quant, ts):
        self.price = pr
        self.quantity = quant
        self.timestamp = ts

    def toString(self):
        print "NODE::::::"
        print "Price: %s" %self.price
        print "quantity: %s" %self.quantity
        print "timestamp: %s" %self.timestamp
        print "::::::::::::::::::::::::::::::"


class FixApplication(fix.Application):
    orderID = 0
    execID = 0
    offerID = 0

    def onCreate(self, sessionID):
        print "In server onCreate"
        return

    def onLogon(self, sessionID):
        print "In server onLogon"
        return

    def onLogout(self, sessionID):
        print "In server onLogout"
        return

    def toAdmin(self, sessionID, message):
        print "In server toAdmin"
        print message
        return

    def fromAdmin(self, sessionID, message):
        print "In server fromAdmin"
        print message
        return

    def toApp(self, sessionID, message):
        print "In server toApp"
        print message
        print '________________'
        return

    def fromApp(self, message, sessionID):
        print 'Server Recieve message from App:'
        print message


        ordType = fix.OrdType()
        message.getField(ordType)

        if ordType.getValue() == fix.OrdType_LIMIT:
            self.limit_order(message, sessionID)
        elif ordType.getValue() == fix.OrdType_MARKET:
            self.market_order(message, sessionID)
        elif ordType.getValue() == fix.OrdType_PEGGED:
            self.pegged_order(message, sessionID)
        else:
            raise fix.IncorrectTagValue(ordType.getField())



    def genOrderID(self):
        self.orderID = self.orderID + 1
        return `self.orderID`

    def genOfferID(self):
        self.offerID = self.offerID + 1
        return `self.offerID`

    def genExecID(self):
        self.execID = self.execID + 1
        return `self.execID`

    def random_times(self, endtime, samples=np.random.randint(5,10)):
        start_time = dt.datetime.now()
        EOD = dt.datetime.combine(endtime.date(), dt.time.max)
        times=[]
        print "EOD: %s" %EOD
        for i in range(samples):
           times.append(start_time + dt.timedelta(
                seconds=random.randint(0, int((EOD - start_time).total_seconds()))))
        return sorted(times)

    def random_times_till(self, endtime, samples=np.random.randint(5,10)):
        start_time = dt.datetime.now()
        times=[]
        print "endtime: %s" %endtime
        for i in range(samples):
           times.append(start_time + dt.timedelta(
                seconds=random.randint(0, int((endtime - start_time).total_seconds()))))
        return sorted(times)

    # :param message:
    # :return:
    def pegged_order(self,message,sessionID):
        global price_cache
        nodes=[]

        #generate random times
        exp_time = fix.ExpireDate()
        message.getField(exp_time)
        xx_time = str(exp_time.getValue())
        expdate = dt.datetime.strptime(xx_time, "%Y%m%d")
        times= self.random_times(expdate, 10)

        symbol = fix.Symbol()
        message.getField(symbol)
        symbol_val = symbol.getValue()
        cummulitive_qty = 0
        average_px = 0.0
        ordQty = fix.OrderQty()
        message.getField(ordQty)
        oq = int(ordQty.getValue())
        offer_id = self.genOfferID()
        #initialize price if it doesn't exist
        price_cache.setdefault(symbol_val, 40*np.random.lognormal(.05,.2))
        for time in times:
            price=price_cache.get(symbol_val) * random.uniform(.985, 1.015)
            price_cache[symbol_val] = price #update price cache
            amt=np.ceil(oq * random.uniform(.2, 1.0))
            nodes.append(FixNode(price,amt,time))
        low = price - random.uniform(0.3, 0.5)
        high = price + random.uniform(0.3, 0.5)
        midpoint = ( low + high ) / 2
        side = fix.Side()
        message.getField(side)
        # print "SIDE: %s" %side.getValue()
        for node in nodes:
            if node.quantity < oq:
                if side.getValue() == fix.Side_BUY:
                    if node.price < midpoint:
                        print "Creating BUY Order for pegged offer."
                        print "Midpoint: %s" %midpoint
                        print node.toString()
                        oq = oq - node.quantity
                        total_spent = ((average_px * cummulitive_qty) + (node.price * node.quantity))
                        cummulitive_qty = cummulitive_qty + node.quantity
                        average_px = total_spent/cummulitive_qty
                        order_id = self.genOrderID()
                        trader = random.choice(trader_names)
                        self.send_execution_report(\
                            message, \
                            symbol, \
                            node.price, \
                            average_px, \
                            node.quantity, \
                            oq, \
                            cummulitive_qty, \
                            side, \
                            fix.OrdType_PEGGED, \
                            trader, \
                            order_id, \
                            offer_id, \
                            exp_time, \
                            node.timestamp, \
                            sessionID)

                if side.getValue() == fix.Side_SELL:
                    if node.price > midpoint:
                        print "Creating SELL Order for pegged offer."
                        print "Midpoint: %s" %midpoint
                        print node.toString()
                        oq = oq - node.quantity
                        total_spent = ((average_px * cummulitive_qty) + (node.price * node.quantity))
                        cummulitive_qty = cummulitive_qty + node.quantity
                        average_px = total_spent/cummulitive_qty
                        order_id = self.genOrderID()
                        trader = random.choice(trader_names)
                        self.send_execution_report(\
                            message, \
                            symbol, \
                            node.price, \
                            average_px, \
                            node.quantity, \
                            oq, \
                            cummulitive_qty, \
                            side, \
                            fix.OrdType_PEGGED, \
                            trader, \
                            order_id, \
                            offer_id, \
                            exp_time, \
                            node.timestamp, \
                            sessionID)
    #     '''
    #     50 time stamps
    #     amounts -> ceiling((input * U(.04 -.08)))
    #     for buys
    #     if by filter(x>=price )
    #     for sells
    #     if by filter(x<=price)

    #     for n in node:
    #         input_amount-=node.amt
    #         last_node=node
    #         if input_amount < 0:
    #             break
    def limit_order(self,message,sessionID):
        nodes=[]

        #generate random times
        exp_time = fix.ExpireDate()
        message.getField(exp_time)
        xx_time = str(exp_time.getValue())
        expdate = dt.datetime.strptime(xx_time, "%Y%m%d")
        times= self.random_times(expdate, 50)

        price = fix.Price()
        message.getField(price)
        price_val = price.getValue()

        symbol = fix.Symbol()
        message.getField(symbol)
        symbol_val = symbol.getValue()

        cummulitive_qty = 0
        average_px = 0.0
        ordQty = fix.OrderQty()
        message.getField(ordQty)
        oq = int(ordQty.getValue())
        offer_id = self.genOfferID()
        side = fix.Side()
        message.getField(side)
        for time in times:
            # generate random prices that fall within a range of the submitted price
            # for the purpose of our logic this assumes limit prices will be somewhat rational.
            node_price = price_val * random.uniform(.92, 1.08)
            amt=np.ceil(oq * random.uniform(0.4, 0.8))
            nodes.append(FixNode(node_price,amt,time))

        for node in nodes:
            if node.quantity < oq:
                if side.getValue() == fix.Side_BUY:
                    if node.price <= price_val:
                        print "Creating BUY Order for limit offer."
                        print node.toString()
                        oq = oq - node.quantity
                        total_spent = ((average_px * cummulitive_qty) + (node.price * node.quantity))
                        cummulitive_qty = cummulitive_qty + node.quantity
                        average_px = total_spent/cummulitive_qty
                        order_id = self.genOrderID()
                        trader = random.choice(trader_names)
                        self.send_execution_report(\
                            message, \
                            symbol, \
                            node.price, \
                            average_px, \
                            node.quantity, \
                            oq, \
                            cummulitive_qty, \
                            side, \
                            fix.OrdType_LIMIT, \
                            trader, \
                            order_id, \
                            offer_id, \
                            exp_time, \
                            node.timestamp, \
                            sessionID)

                if side.getValue() == fix.Side_SELL:
                    if node.price >= price_val:
                        print "Creating SELL Order for limit offer."
                        print node.toString()
                        oq = oq - node.quantity
                        total_spent = ((average_px * cummulitive_qty) + (node.price * node.quantity))
                        cummulitive_qty = cummulitive_qty + node.quantity
                        average_px = total_spent/cummulitive_qty
                        order_id = self.genOrderID()
                        trader = random.choice(trader_names)
                        self.send_execution_report(\
                            message, \
                            symbol, \
                            node.price, \
                            average_px, \
                            node.quantity, \
                            oq, \
                            cummulitive_qty, \
                            side, \
                            fix.OrdType_LIMIT, \
                            trader, \
                            order_id, \
                            offer_id, \
                            exp_time, \
                            node.timestamp, \
                            sessionID)



    def market_order(self,message,sessionID):
        global price_cache
        nodes=[]

        exp_time = fix.ExpireDate()
        message.getField(exp_time)

        #generate random times
        end_time = dt.datetime.now() + dt.timedelta(minutes=5)
        times= self.random_times_till(end_time, 20)

        symbol = fix.Symbol()
        message.getField(symbol)
        symbol_val = symbol.getValue()

        cummulitive_qty = 0
        average_px = 0.0
        ordQty = fix.OrderQty()
        message.getField(ordQty)
        oq = int(ordQty.getValue())
        offer_id = self.genOfferID()
        # set a price if it doesn't exist in the cache.
        price_cache.setdefault(symbol_val, 40*np.random.lognormal(.05,.2))
        #update the price in the market for each new offer
        price=price_cache.get(symbol_val) * random.uniform(.992, 1.008)
        price_cache[symbol_val] = price #update price cache
        for time in times:
            #small variation in price
            node_price=price_cache.get(symbol_val) * random.uniform(.992, 1.008)
            amt=np.ceil(oq * random.uniform(.2, 0.4))
            nodes.append(FixNode(node_price,amt,time))
        side = fix.Side()
        message.getField(side)
        for node in nodes:
            if node.quantity < oq:
                if side.getValue() == fix.Side_BUY:
                    if node.price <= price:
                        print "Creating BUY Order for pegged offer."
                        print node.toString()
                        oq = oq - node.quantity
                        total_spent = ((average_px * cummulitive_qty) + (node.price * node.quantity))
                        cummulitive_qty = cummulitive_qty + node.quantity
                        average_px = total_spent/cummulitive_qty
                        order_id = self.genOrderID()
                        trader = random.choice(trader_names)
                        self.send_execution_report(\
                            message, \
                            symbol, \
                            node.price, \
                            average_px, \
                            node.quantity, \
                            oq, \
                            cummulitive_qty, \
                            side, \
                            fix.OrdType_MARKET, \
                            trader, \
                            order_id, \
                            offer_id, \
                            exp_time, \
                            node.timestamp, \
                            sessionID)

                if side.getValue() == fix.Side_SELL:
                    if node.price >= price:
                        print "Creating SELL Order for pegged offer."
                        print node.toString()
                        oq = oq - node.quantity
                        total_spent = ((average_px * cummulitive_qty) + (node.price * node.quantity))
                        cummulitive_qty = cummulitive_qty + node.quantity
                        average_px = total_spent/cummulitive_qty
                        order_id = self.genOrderID()
                        trader = random.choice(trader_names)
                        self.send_execution_report(\
                            message, \
                            symbol, \
                            node.price, \
                            average_px, \
                            node.quantity, \
                            oq, \
                            cummulitive_qty, \
                            side, \
                            fix.OrdType_MARKET, \
                            trader, \
                            order_id, \
                            offer_id, \
                            exp_time, \
                            node.timestamp, \
                            sessionID)


    def send_execution_report(self, message, symbol, price, average_px, quantity, leaves_qty, cummulitive_qty, side, order_type, trader, order_id, offer_id, exp_time, trade_date, sessionID):
        print "Preparing to send execution report."
        print "Expire Time: %s" %exp_time
        print "Trade Date: %s" %trade_date
        beginString = fix.BeginString()
        clOrdID = fix.ClOrdID()
        message.getField(clOrdID)
        executionReport = fix.Message()
        executionReport.getHeader().setField(beginString)
        executionReport.getHeader().setField(fix.MsgType(fix.MsgType_ExecutionReport))
        executionReport.setField(clOrdID)
        executionReport.setField(exp_time)
        executionReport.setField(fix.AvgPx(average_px))
        executionReport.setField(fix.CumQty(cummulitive_qty))
        executionReport.setField(fix.ExecID(self.genExecID()))
        executionReport.setField(fix.ExecTransType(fix.ExecTransType_NEW))
        executionReport.setField(fix.ExecType(fix.ExecType_PARTIAL_FILL))
        executionReport.setField(fix.LeavesQty(leaves_qty))
        executionReport.setField(fix.OrderID(order_id))
        executionReport.setField(fix.OrderQty(quantity))
        executionReport.setField(fix.OrdStatus(fix.OrdStatus_FILLED))
        executionReport.setField(fix.OrdType(order_type))
        executionReport.setField(fix.Price(price))
        executionReport.setField(fix.Text(trader))
        executionReport.setField(fix.TradeDate(trade_date.strftime('%Y%m%d')))
        executionReport.setField(fix.EncodedText(trade_date.strftime("%H:%M:%S")))
        # executionReport.setField(fix.TransactTime(trade_date))
        # executionReport.setField(fix.TransactTime(timee.mktime(trade_date.timetuple())))
        executionReport.setField(side)
        executionReport.setField(symbol)

        print "Finishing contructing execution report body."

        try:
            fix.Session.sendToTarget(executionReport, sessionID)

        except fix.SessionNotFound, e:
            return



def main(file_name):
    try:
        settings = fix.SessionSettings(file_name)
        application = FixApplication()
        storeFactory = fix.FileStoreFactory(settings)
        logFactory = fix.FileLogFactory(settings)
        print 'creating acceptor'

        acceptor = fix.SocketAcceptor(application, storeFactory, settings, logFactory)
        print 'starting acceptor'
        acceptor.start()

        while 1:
            time.sleep(1)
    except (fix.ConfigError, fix.RuntimeError), e:
        print e


if __name__ == '__main__':
    main(CONF_FILE_PATH)
















# #    @NotImplemented
#     def market_order(self, message, sessionID):
#         '''
#         take a fix message
#         make 10 timestamps node in increments of one or 2 seconds


#         amounts -> ceiling((input * U(.1 -.2)))
#         last_node
#         for n in node:
#             input_amount-=node.amt
#             last_node=node
#             if input_amount < 0:
#                 break
#         last_node+=input_amount
#         send the set as messeges




#         for every price make a node

#         :param message:
#         :return:
#         '''
#         date = dt.datetime.now()
#         nodes = []
#         for i in range(10):
#             seconds = np.random.randint(3)
#             # times.append(date+dt.timedelta(seconds=seconds))
#             ts = date + dt.timedelta(seconds=seconds)
#             amt = np.ceil(int(message.getField(fix.OrderQty())) * np.random.random(.1, .2))
#             price = 40 * np.random.lognormal(.05, .15)
#             nodes.append(FixNode(price, amt, ts))
#         input_amt = int(message.getFeild(fix.OrderQty()))
#         last_node = None
#         num = 0
#         for i in range(len(nodes)):
#             input_amt -= nodes[i].amt
#             last_node = nodes[i]
#             num = i
#             if input_amt < 0:
#                 break
#         last_node += input_amt

#         # construct a set of fix messages to send back
#         # now send over the number of nodes until we hit the node that broke us

#         for i in nodes[:num]:
#             executionReport = fix.Message()
#             executionReport.getHeader().setField(fix.MsgType(fix.MsgType_ExecutionReport))
#             executionReport.getHeader().setField(fix.BeginString())
#             executionReport.setField(fix.OrderID(self.genOrderID()))
#             executionReport.setField(fix.ExecID(self.genExecID()))
#             # executionReport.setField(fix.OrdStatus(fix.))