# TechStack

 * Python backend with flask.
 * JavaScript front end with ExtJS 6.0.
 * MongoDB for the Database.
 * Python virtualenv to manage python dependencies.
 * Sencha Cmd 6 (most recent version) to build sass and check javascript for basic syntax and errors.
 * RabbitMQ for messaging between internal applications.
 * QuickFIX for messaging between internal applications and exchange.


Currently I took an example sencha ext demo template to use as the basis for our frontend. I will get rid of most of the UI code needed.  The tradeweb_env directory is a python virtualenv with all python dependencies included. I set up a schema for a trades table and have it working. This will need a few tweeks in the controller logic to take string dates and convert them to python dates during insertion.

  To start the app:
  cd CSOR4995TradeWeb
  source <os>_venv/bin/activate
  mongod -dbpath
  python manage.py runserver

_________________________________________________________________

Checkpoint 1 Report can be found in documentation folder.

jeb2239, ud2122, mah2250

_________________________________________________________________

#Checkpoint 2
### Prerequisites

 * RabbitMQ
   * [Erlang](http://www.erlang.org/)
 * quickfix
   * pip install quickfix
   * pip install pika (rabbitmq)


### To Run

#### Scripted Start

This seems to have a bug right now but it is still useful in understanding the application startup flow.

 * Source the environment file corresponding to your system.

    source bin/fc16_env.sh

 * Execute start script

    bin/systemCtl --start

 * Reference logs as needed under ./var/logs

#### Manual Start

It is often useful to restart all processes when making changes that require restarting any processes. This is because sessions exist that cause the system to stop communicating when one service drops out and comes back up. This can be fixed but is outside the scope of this project.

 * Start Rabbit MQ First. [See this tutorial](https://www.rabbitmq.com/install-standalone-mac.html)

    The location of the rabbitmq_server will depend on your system os. All variations should be located under lib.

    pushd lib/rabbitmq_server-3.5.6/sbin
    ./rabbitmq-server

 * Then start mongodb

    mongod -dbpath var/dbdata

 * Start the Exchange (FIX Server)

    pushd apps/fixserver
    python server.py
    popd

 * Start the FIX Client

    pushd apps/fixclient
    python fix.py -c primary.conf
    python fix.py -c secondary.conf
    popd

 * Start TradeWeb

    python manage.py runserver
    python manage.py runbackup

 * Start Clearing house

    pushd apps/clearing_house
    python clearing.py -c clearing.conf
    popd

 * Start HeartBeat Server

    pushd apps/heartbeat
    python heartbeat.py
    popd



 * Open browser/View site

    Located at [http://localhost:5000](http://localhost:5000)

#### Debugging

 * FIX

    After starting the exchange and fixclient applications some basic messaging will print out in the console that should help to understand the messaging flow. However, debugging information in the console is minimal. For more info look at the FIX.4.2-SRVR-CLNT.event.current.log and FIX.4.2-CLNT-SRVR.event.current.log under var/logs/fixserver and var/logs/ respectively.

    This will provide more details such as missing required fields

    20151112-05:25:09.000 : Message 3 Rejected: Conditionally Required Field Missing:44

Notes:
Random prices will be spit out between 25 and 55


