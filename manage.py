# Set the path
import os, sys
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from apps.tradeweb import manager


if __name__ == "__main__":
    manager.run()